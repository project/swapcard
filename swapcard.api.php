<?php

/**
 * @file
 * Hooks provided by the Swapcard module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter current API request, either fields or args or similar.
 *
 * @param array $fields
 *   Current fields array for GraphQl.
 * @param string $callback
 *   Swapcard API callback name.
 * @param array $callback_args
 *   Swapcard API callback's params.
 */
function hook_swapcard_request_alter(&$fields, &$callback, &$callback_args) {
  // Example: Add some more fields that are not included by default,
  // to "exhibitors" Swapcard API callback.
  if ($callback == 'exhibitors') {
    foreach ($fields['events']['nodes'] as &$node) {
      $node['availableLocations'] = [
        'name',
        'id',
        'capacity',
        'category',
        'isBooth',
        'isVirtual',
      ];
    }
  }
}
