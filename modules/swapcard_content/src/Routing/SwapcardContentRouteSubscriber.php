<?php

namespace Drupal\swapcard_content\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class SwapcardContentRouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    // Change form for the 'swapcard.config' route
    // to Drupal\swapcard_content\Form\SwapcardContentConfigForm.
    if ($route = $collection->get('swapcard.config')) {
      $route->setDefault('_form', 'Drupal\swapcard_content\Form\SwapcardContentConfigForm');
    }
  }

}
