<?php

namespace Drupal\swapcard_content\Element;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\Fieldset;

/**
 * Provides a form element with Flickr options.
 *
 * Usage example:
 * @code
 * $form['swapcard_content_field'] = [
 *   '#type' => 'swapcard_content_custom_field',
 *   '#title' => t('Custom field'),
 *   '#default_value' => (array) $values,
 * ];
 * @endcode
 *
 * @FormElement("swapcard_content_custom_field")
 */
class SwapcardCustomField extends Fieldset {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {

    $info = parent::getInfo();
    $process = [__CLASS__, 'processCustomField'];
    array_unshift($info['#process'], $process);
    return $info;
  }

  /**
   * Define and append children for this element.
   */
  public static function processCustomField(array &$element, FormStateInterface $form_state, array &$complete_form) {

    $element['drupal_field_name'] = [

      '#title' => t('Drupal field'),
      '#description' => t("Select field to map with Swapcard custom field. Type <strong>field's label</strong> in this input to get autocomplete results."),
      '#type' => 'entity_autocomplete',
      '#target_type' => 'field_config',
      // The #default_value can be either an entity object
      // or an array of entity objects.
      '#default_value' => $element['#default_value']['drupal_field_name'] ?? NULL,
      '#multiple' => FALSE,
      '#maxlength' => '2048',
      '#attributes' => [
        'id' => Html::getId('swapcard-drupal-field-wrapper'),
      ],
    ];

    $element['swapcard_field_id'] = [
      '#type' => 'textfield',
      '#title' => t('Swapcard field ID'),
      '#description' => t('As found in Swapcard app (when editing field itself). Looks similar to this <em>RmllbGREZWZpbml0aW9uXzM1NjUwNQ==</em>'),
      '#default_value' => $element['#default_value']['swapcard_field_id'] ?? NULL,
      '#maxlength' => '1024',
    ];

    return $element;

  }

}
