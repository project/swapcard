<?php

namespace Drupal\swapcard_content\Commands;

use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Queue\QueueWorkerManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\queue_ui\QueueUIBatchInterface;
use Drush\Commands\DrushCommands;
use Drush\Exceptions\UserAbortException;

/**
 * Provide Drush commands for Swapcard content module.
 */
class SwapcardCommands extends DrushCommands {

  use StringTranslationTrait;

  /**
   * Queue worker manager.
   *
   * @var \Drupal\Core\Queue\QueueWorkerManagerInterface
   */
  protected $queueWorkerManager;

  /**
   * Queue UI Batch service.
   *
   * @var \Drupal\queue_ui\QueueUIBatchInterface
   */
  protected $queueUiBatch;

  /**
   * Drush command constructor.
   *
   * @param \Drupal\Core\Queue\QueueWorkerManagerInterface $queue_worker_manager
   *   Queue worker manager.
   * @param \Drupal\queue_ui\QueueUIBatchInterface $queue_ui_batch
   *   Queue UI Batch service.
   */
  public function __construct(
    QueueWorkerManagerInterface $queue_worker_manager,
    QueueUIBatchInterface $queue_ui_batch) {
    parent::__construct();
    $this->queueWorkerManager = $queue_worker_manager;
    $this->queueUiBatch = $queue_ui_batch;
  }

  /**
   * Sync Event(s) and its entities.
   *
   * @command swapcard_content:sync
   * @aliases swc-sync
   * @usage swc-sync
   */
  public function sync(string $events = NULL, $options = ['batches' => NULL]) {
    $plugin = $this->queueWorkerManager->createInstance('swapcard_content_queue_swapcard_event');
    $factory = $plugin->queueFactory->get('swapcard_content_queue_swapcard_request');
    $params = [
      'batch' => FALSE,
    ];
    if ($events) {
      $params['events'] = explode(',', trim($events));
    }

    if ($factory->createItem($params)) {
      if ($factory->numberOfItems()) {
        // Send some log/message about start of queues.
        $status = $this->t('@plugin', ['@plugin' => $plugin::START_LINE]);
        $plugin->logger->get('swapcard_content')->notice($status);
        // Claim init, helper request.
        $plugin->claimItems('swapcard_content_queue_swapcard_request');
      }
    }
  }

  /**
   * Purge Event(s) and its entities.
   *
   * @param string|null $event_id
   *   ID of the event being purged.
   * @param array $options
   *   Options being saved.
   *
   * @option batches Any additional batches to append.
   * @command swapcard_content:purge
   * @aliases swc-purge
   * @usage swc-purge RXZlbnRfMTIwODY3OQ== --batches='entity_reference_revisions_orphan_purger'
   */
  public function purge(string $event_id = NULL, $options = ['batches' => NULL]) {

    // Add operations and start to batch process.
    if ($event_id) {
      $this->doPurge($event_id, $options);
    }
    else {
      if ($plugin = $this->queueWorkerManager->createInstance('swapcard_content_queue_swapcard_entities')) {
        $events = $plugin->config->get('events');
        if (!empty($events)) {
          foreach ($events as $event) {
            $this->doPurge($event, $options);
          }
        }
      }
    }
  }

  /**
   * Helper function to start a batch process.
   *
   * @param \Drupal\Core\Batch\BatchBuilder $batch_definition
   *   The batch that needs to be processed.
   */
  protected function startBatch(BatchBuilder $batch_definition): void {
    // Set and configure the batch.
    batch_set($batch_definition->toArray());
    $batch = & batch_get();
    $batch['progressive'] = TRUE;

    // Process the batch.
    drush_backend_batch_process();
  }

  /**
   * Purge event and al of its entities.
   *
   * @param string $event_id
   *   ID of the event being purged.
   * @param array $options
   *   Drush command options array.
   */
  private function doPurge(string $event_id, array $options = []) {

    $plugin = $this->queueWorkerManager->createInstance('swapcard_content_queue_swapcard_entities');
    $event_node_properties['field_swapcard_event_id'] = $event_id;
    if ($event_node = $plugin->existingEntity('node', $event_node_properties)) {

      $data = [
        'id' => $event_id,
        'event_entity' => $event_node,
        'entities' => [],
        'callback' => 'delete',
      ];

      $entities_config = $plugin->checkEventEntities();
      if (!empty($entities_config)) {
        if ($entities_config['entity_key'] == 'swapcard_exhibitor') {
          if (isset($entities_config['sessions_key'])) {
            $data['entities'][] = 'swapcard_session';
          }
          $data['entities'][] = 'swapcard_exhibitor';
        }
        elseif ($entities_config['entity_key'] == 'swapcard_session') {
          $data['entities'][] = 'swapcard_session';
        }
      }

      $entities = [];
      foreach ($data['entities'] as $bundle) {
        $entities += $plugin->matchEntities($data, $bundle, $event_node);
      }

      if (!empty($entities)) {

        $entities[$event_node->id()] = [
          'entity' => $event_node,
          'data' => [
            'id' => $event_id,
            'event_entity' => $event_node,
            'callback' => 'delete',
          ],
        ];

        // Add the other modules ability to extend purging data.
        $plugin->moduleHandler->alter('swapcard_content_purge', $entities);

        $confirmation_title = $this->t('Purge event "@title" and @count of its entities listed above?', [
          '@title' => $event_node->getTitle(),
          '@count' => count($entities),
        ]);

        $confirmation_list = [];
        $index = 0;
        $diff = count($entities) - 151;
        foreach ($entities as $entity) {
          if ($index <= 149) {
            $confirmation_list[] = $entity['entity']->label() . ' [' . $entity['entity']->bundle() . ']';
            $index++;
          }
          // $plugin->queueFactory->get('swapcard_content_queue_swapcard_delete')->createItem($entity);
        }

        $listing = $this->io()->listing($confirmation_list);
        $this->output()->writeln($listing);

        if (!empty($confirmation_list) && $diff > $index) {
          $confirmation = $this->io()->comment($this->t('... and @diff more.', ['@diff' => $diff]));
          $this->output()->writeln($confirmation);
        }

        $confirm = $this->io()->confirm(dt($confirmation_title), TRUE);

        // Break here, user have chosen "no" at the prompt.
        if (!$confirm) {
          throw new UserAbortException();
        }

        foreach ($entities as $entity) {
          $plugin->queueFactory->get('swapcard_content_queue_swapcard_delete')->createItem($entity);
        }

        $this->output()->writeln($this->t('Processing delete batches...'));

        if ($config = $plugin->configFactory->getEditable('swapcard_content.settings')) {

          if (!$config->get('cron_verbose')) {
            $this->io()->setVerbosity($this->output()::VERBOSITY_QUIET);
          }

          $filtered_events = array_filter($config->get('events'), function ($value) use ($event_id) {
            return $value != $event_id;
          });

          $config->set('events', array_values($filtered_events));
          $config->save();
        }

        if (isset($options['batches'])) {
          $batches = explode(',', $options['batches']);
          array_unshift($batches, 'swapcard_content_queue_swapcard_delete');
        }
        else {
          $batches = [
            'swapcard_content_queue_swapcard_delete',
          ];
        }

        $batch = new BatchBuilder();
        foreach ($batches as $b) {
          $batch->addOperation([$this->queueUiBatch, 'step'], [$b]);
        }

        $this->startBatch($batch);
      }
    }
  }

}
