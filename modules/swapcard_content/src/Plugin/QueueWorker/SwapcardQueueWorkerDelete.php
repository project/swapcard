<?php

namespace Drupal\swapcard_content\Plugin\QueueWorker;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\media\MediaInterface;

/**
 * Creates QueueWorker for deleting over Swapcard ecosystem.
 *
 * @QueueWorker(
 *   id = "swapcard_content_queue_swapcard_delete",
 *   title = @Translation("Swapcard Delete entities"),
 *   cron = {"time" = 90}
 * )
 */
class SwapcardQueueWorkerDelete extends SwapcardQueueWorkerSessions {

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {

    if (empty($data)) {
      return;
    }

    if (isset($data['entity'])) {
      $this->doDelete($data);
    }
    else {
      foreach ($data as $item) {
        $this->doDelete($item);
      }
    }
  }

  /**
   * Delete entity.
   *
   * @param array $item
   *   An array with properties of a item being deleted.
   */
  private function doDelete(array $item) {
    $entity = NULL;
    if ($item instanceof ContentEntityInterface) {
      $entity = $item;
    }
    elseif (is_array($item)) {
      if (isset($item['entity']) && $item['entity'] instanceof ContentEntityInterface) {
        $entity = $item['entity'];
      }
    }
    if (!$entity) {
      return;
    }

    if (!$this->config->get('dry_run')) {

      // Delete this entity.
      $entity->delete();

      // And notify (in shell only).
      $this->notify($entity, 'Deleted');

      // A special case for Session <> Speaker.
      $entities = $this->checkEventEntities();
      if (isset($entities['speakers_key']) && $entity->bundle() == 'swapcard_session' && isset($item['data'])) {
        $this->orphanSpeakers($item['data'], $entity);
      }

      if (!$entity instanceof MediaInterface) {

        // Swapcard content media plugin handling.
        // Take care of some Media deleting if marked.
        $has_media = $this->hasMedia($item['data'], $entity->bundle());

        if (isset($has_media['queue_name']) && isset($has_media['origin'])) {
          $media_plugin = $this->queueManager->createInstance($has_media['queue_name']);
          $media_plugin->processMedia($item['data'], ['type' => $entity->bundle()], NULL, $entity->label(), $entity);
        }
      }
    }
    else {
      // Notify about dry run.
      if ($this->config->get('dry_run')) {
        $status = $this->t('Running in Dry run mode therefore @entity_type entity @type "@title" was not deleted, but only marked for deleting because it is not attached to its parent anymore and has no other parents within Drupal.', $status_params);
        // And notify (in shell only).
        $this->notify($entity, 'Deleted', $status);
      }
    }
  }

}
