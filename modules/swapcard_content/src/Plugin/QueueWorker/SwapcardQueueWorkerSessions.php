<?php

namespace Drupal\swapcard_content\Plugin\QueueWorker;

use Drupal\Component\Utility\Crypt;

use Drupal\node\NodeInterface;

/**
 * Creates or updates nodes based on results retrived by API call/response.
 *
 * @QueueWorker(
 *   id = "swapcard_content_queue_swapcard_session",
 *   title = @Translation("Swapcard Sessions"),
 *   cron = {"time" = 120}
 * )
 */
class SwapcardQueueWorkerSessions extends SwapcardQueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function prepareItem(array $data, array $fields, NodeInterface $node = NULL) {

    // Invoke parent method.
    // this prepares a lot of default fields, hence here only specifics.
    $node_data = parent::prepareItem($data, $fields);

    // Add the other modules ability to make some custom logic or IA changes.
    $this->moduleHandler->alter('swapcard_content_prepare', $data, $node_data, $fields, $node);

    // Try to purge any obsolete speakers.
    // This HAS TO go as the first thing on this function.
    $entities = $this->checkEventEntities();
    if (isset($entities['speakers_key']) && $this->config->get('purge')) {
      if (isset($node_data['existing_entity']) && $node_data['existing_entity'] instanceof NodeInterface) {
        $this->orphanSpeakers($data, $node_data['existing_entity']);
      }
    }

    foreach ($fields as $drupal_field_name => $swapcard_field) {
      switch ($swapcard_field['type']) {

        case 'entity_reference':

          switch ($drupal_field_name) {

            case 'field_swapcard_exhibitors':

              if (isset($data['exhibitors']) && !empty($data['exhibitors'])) {

                foreach ($data['exhibitors'] as $delta => $exhibitor) {
                  if (isset($exhibitor['id'])) {
                    $existing_exhibitor_properties = [
                      'field_swapcard_exhibitor_id' => $exhibitor['id'],
                    ];
                    $existing_exhibitor = $this->existingEntity('node', $existing_exhibitor_properties);

                    if ($existing_exhibitor instanceof NodeInterface) {
                      $node_data[$drupal_field_name][$delta]['target_id'] = $existing_exhibitor->id();
                    }
                    else {
                      $node_data[$drupal_field_name][$delta] = NULL;
                    }
                  }
                  else {
                    $node_data[$drupal_field_name][$delta] = NULL;
                  }
                }
              }
              else {
                $node_data[$drupal_field_name] = NULL;
              }
              break;

            case 'field_swapcard_speakers':

              if (isset($entities['speakers_key']) && isset($data['speakers']) && !empty($data['speakers'])) {

                foreach ($data['speakers'] as $delta => $speaker) {
                  if (isset($speaker['id'])) {
                    if ($speaker_id = $this->matchSpeakers($speaker)) {

                      $existing_speaker_properties = [
                        'type' => 'swapcard_speaker',
                        'field_swapcard_speaker_id' => $speaker_id,
                      ];
                      $existing_speaker = $this->existingEntity('node', $existing_speaker_properties);

                      if ($existing_speaker instanceof NodeInterface) {
                        $node_data[$drupal_field_name][$delta]['target_id'] = $existing_speaker->id();
                      }
                      else {
                        $node_data[$drupal_field_name][$delta] = NULL;
                      }
                    }
                    else {
                      $node_data[$drupal_field_name][$delta] = NULL;
                    }
                  }
                  else {
                    $node_data[$drupal_field_name][$delta] = NULL;
                  }
                }
              }
              else {
                $node_data[$drupal_field_name] = NULL;
              }
              break;

          }
          break;
      }
    }

    return $node_data;
  }

  /**
   * An effort to try to match Speakers for updates and othe relations.
   *
   * A BIG note - speakers do NOT have unique ID across Events.
   * Even though it IS the only entity that is assigneable across events.
   * So instead of some unique ID that all the other entities have,
   * here we are trying to do some very unstable matching by email or name.
   * Either can be changed by user indeed. Point is that we do not want
   * repeating Swapcard speaker nodes in our Drupal.
   *
   * @param array $data
   *   Associative array with Speaker's data.
   *
   * @return string
   *   Some kind of unique hash value for this speaker.
   */
  public function matchSpeakers(array $data) {

    // Email is already unique, so we are good.
    if (isset($data['email']) && !empty($data['email'])) {
      return Crypt::hashBase64($data['email']);
    }
    // Helaas, no email value for this speaker,
    // let's make some effort to identify it by name (very unstable).
    else {
      $origin = isset($this->config->get('node_types')['swapcard_speaker']['title']) && isset($this->config->get('node_types')['swapcard_speaker']['title']['origin']) ? $this->config->get('node_types')['swapcard_speaker']['title']['origin'] : [];
      if (!empty($origin)) {
        $title_array = $this->processTitle($origin, $data);
        if (!empty($title_array)) {
          return Crypt::hashBase64(implode('', $title_array));
        }
      }
    }
  }

  /**
   * Add speakers to queue.
   *
   * @param array $data
   *   Associative array containing Session properties.
   * @param bool $batch
   *   A flag for to determine if this is batch from UI or Cron run.
   */
  public function queueSpeakers(array $data, bool $batch = FALSE) {

    $speakers_queue = [];

    foreach ($data as $event_id => $event) {

      if (isset($event['swapcard_session']) && !empty($event['swapcard_session'])) {

        foreach ($event['swapcard_session'] as $session) {
          if (isset($session['speakers']) && !empty($session['speakers'])) {
            foreach ($session['speakers'] as $speaker) {
              $speaker['node_type'] = 'swapcard_speaker';
              $speaker['event_id'] = $event_id;
              $speaker['session_id'] = $session['id'];
              $speakers_queue[$speaker['id']] = $speaker;
            }
          }
        }
      }
    }

    // Add speakers to queue.
    if (!empty($speakers_queue)) {
      $this->queueFactory->get('swapcard_content_queue_swapcard_speaker')->createItem($speakers_queue);
    }
  }

  /**
   * Deleting obsolete Swapcard speakers.
   *
   * @param array $data
   *   Associative array containing Session properties.
   * @param \Drupal\node\NodeInterface $session_node
   *   Saved Swapcard session node object.
   */
  protected function orphanSpeakers(array $data, NodeInterface $session_node) {

    if ($session_node->hasField('field_swapcard_speakers') && !empty($session_node->get('field_swapcard_speakers')->getValue())) {

      $queue_data = [];

      $speaker_ids = [];
      if (isset($data['speakers']) && !empty($data['speakers'])) {
        foreach ($data['speakers'] as $speaker) {
          if ($speaker_id = $this->matchSpeakers($speaker)) {
            $speaker_ids[] = $speaker_id;
          }
        }
      }

      $queue_data = [];

      foreach ($session_node->get('field_swapcard_speakers')->getValue() as $value) {

        if (isset($value['target_id']) && !empty($value['target_id'])) {

          $speaker_entity = $this->entityTypeManager->getStorage('node')->load($value['target_id']);
          if ($speaker_entity instanceof NodeInterface) {

            $nid = $this->config->get('dry_run') ? NULL : $session_node->id();

            $query = $this->querySpeakers($nid, $speaker_entity->id());

            $speaker_id = $speaker_entity->hasField('field_swapcard_speaker_id') && !empty($speaker_entity->get('field_swapcard_speaker_id')->getValue()) ? $speaker_entity->get('field_swapcard_speaker_id')->getValue()[0]['value'] : NULL;

            if (empty($query) && !in_array($speaker_id, $speaker_ids)) {
              $queue_data[$value['target_id']] = [
                'entity' => $speaker_entity,
                'data' => $data,
              ];
              // $this->queueFactory->get('swapcard_content_queue_swapcard_delete')->createItem($queue_data[$value['target_id']]);
            }
          }
        }
      }

      if (!empty($queue_data)) {
        if (!isset($data['batch']) || $data['batch'] == FALSE) {
          $this->queueFactory->get('swapcard_content_queue_swapcard_delete')->createItem($queue_data);
          $this->claimItems('swapcard_content_queue_swapcard_delete');
        }
        else {
          $this->queueFactory->get('swapcard_content_queue_swapcard_delete')->createItem($queue_data);
          // $this->queueUi->batch(['swapcard_content_queue_swapcard_delete']);
        }
      }
    }
  }

  /**
   * Finding Speakers reverse way.
   *
   * @param string $session_id
   *   Parent Swacard session node nid.
   * @param string $speaker_id
   *   Target Swapcard speaker node nid.
   * @param string $operator
   *   Query operator for matching/not session_id.
   *
   * @return array
   *   Associative array containing matches keyed with Speaker node nid.
   */
  protected function querySpeakers(string $session_id = NULL, string $speaker_id = NULL, string $operator = '!=') {
    $query = $this->database->select('node_field_data', 'n');
    $query->leftJoin('node__field_swapcard_speakers', 'd', 'n.nid = d.field_swapcard_speakers_target_id');
    $query->addField('n', 'nid');
    $query->addField('n', 'title');
    $query->addField('d', 'entity_id');
    $query->condition('n.type', 'swapcard_speaker');

    if ($session_id) {
      $query->condition('d.entity_id', $session_id, $operator);
    }
    else {
      $query->condition('d.entity_id', $session_id, 'IS NOT NULL');
    }
    if ($speaker_id) {
      $query->condition('d.field_swapcard_speakers_target_id', $speaker_id);
    }
    return $query->execute()->fetchAllKeyed(2, 1);
  }

}
