<?php

namespace Drupal\swapcard_content\Plugin\QueueWorker;

use Drupal\node\NodeInterface;

/**
 * Creates QueueWorker for a data request.
 *
 * @QueueWorker(
 *   id = "swapcard_content_queue_swapcard_entities",
 *   title = @Translation("Swapcard Content Entities"),
 *   cron = {"time" = 60}
 * )
 */
class SwapcardQueueWorkerEntities extends SwapcardQueueWorkerSessions {

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {

    if (isset($data['entities']) && !empty($data['entities'])) {

      if (isset($data['callback']) && !empty($data['callback']) && $data['event_entity'] instanceof NodeInterface) {
        $batches_data = [];
        foreach ($data['entities'] as $entity_bundle) {
          if ($data['callback'] == 'delete') {
            $delete_entities = $this->matchEntities($data, $entity_bundle, $data['event_entity'], FALSE);
            if (is_array($delete_entities)) {
              $batches_data += $delete_entities;
            }
          }
        }

        // Add the other modules ability to extend purging data.
        $this->moduleHandler->alter('swapcard_content_purge', $batches_data);

        $state_data = [
          'data' => $data,
          'batches_data' => $batches_data,
        ];

        // Set data for deletion in the state, for the next batch.
        $this->state->set('swapcard_content_entities', $state_data);
      }
    }
  }

  /**
   * Match Exhibitors and/or Sessions belonging to the Event.
   *
   * @param array $data
   *   Associative array with event's values.
   * @param string $node_type
   *   The type of entity being processed.
   * @param \Drupal\node\NodeInterface $event_node
   *   Existing Swapcard event node object.
   * @param bool $batch
   *   If true, this callback will trigger batch run.
   */
  public function matchEntities(array $data, string $node_type, NodeInterface $event_node, bool $batch = TRUE) {
    $batches = [];
    $existing_ids = [];

    $existing_nodes_properties = [
      'type' => $node_type,
    ];

    $existing_nodes_properties['field_swapcard_events.entity:node.nid'] = $event_node->id();
    $existing_nodes = $this->existingEntity('node', $existing_nodes_properties, FALSE);
    if (!empty($existing_nodes)) {
      $existing_ids = [];
      foreach ($existing_nodes as $existing_node) {
        if ($existing_node->bundle() == $node_type) {
          $unique_field_name = 'field_' . $existing_node->bundle() . '_id';
          if ($existing_node->hasField($unique_field_name) && !empty($existing_node->get($unique_field_name)->getValue())) {
            $existing_id = $existing_node->get($unique_field_name)->getValue()[0]['value'] ?? NULL;
            if ($existing_id) {
              $existing_ids[$existing_id] = $existing_node;
            }
          }
        }
      }
    }

    if (!empty($existing_ids)) {

      $compare_data = !isset($data[$node_type]) ? [] : $data[$node_type];
      $diff = array_diff(array_keys($existing_ids), array_keys($compare_data));

      if (!empty($diff)) {
        foreach ($diff as $dd) {
          if ($existing_ids[$dd] instanceof NodeInterface) {

            $batches[$dd] = [
              'entity' => $existing_ids[$dd],
              'data' => $data,
            ];

            // Take care of any media here.
            $has_media = $this->hasMedia($data, $existing_ids[$dd]->bundle());

            if (isset($has_media['queue_name']) && isset($has_media['origin'])) {
              $media_plugin = $this->queueManager->createInstance($has_media['queue_name']);
              $delete_media = $media_plugin->processMedia($data, ['type' => $existing_ids[$dd]->bundle()], NULL, $existing_ids[$dd]->label(), $existing_ids[$dd]);
              if (!empty($delete_media)) {
                foreach ($delete_media as $media_id => $media_data) {
                  if (isset($media_data['entity']) && isset($media_data['data'])) {
                    $batches[$media_id] = $media_data;
                  }
                }
              }
            }
          }
        }
      }
    }
    return $batches;
  }

}
