<?php

namespace Drupal\swapcard_content\Plugin\QueueWorker;

/**
 * Creates QueueWorker for Swapcard events.
 *
 * @QueueWorker(
 *   id = "swapcard_content_queue_swapcard_exhibitor",
 *   title = @Translation("Swapcard Exhibitors"),
 *   cron = {"time" = 120}
 * )
 */
class SwapcardQueueWorkerExhibitors extends SwapcardQueueWorkerBase {}
