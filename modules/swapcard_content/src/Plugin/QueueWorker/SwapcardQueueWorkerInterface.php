<?php

namespace Drupal\swapcard_content\Plugin\QueueWorker;

use Drupal\node\NodeInterface;

/**
 * Definition of SwapcardQueueWorkerInterface.
 */
interface SwapcardQueueWorkerInterface {

  /**
   * Base arguments for Graphql query callback.
   *
   * Those should be available for all of the Graphql queries.
   *
   * @var array
   */
  const BASE_ARGS = [
    'page' => 1,
    'pageSize' => 0,
  ];

  /**
   * Default path for test YML dump.
   *
   * @var string
   */
  const DUMP_PATH = 'public://swapcard/tests';

  /**
   * Entity's title can never be empty.
   *
   * @var string
   */
  const PLACEHOLDER_TITLE = '[Placeholder Title] Cannot be empty';

  /**
   * Batch process info line/log.
   *
   * @var string
   */
  const START_LINE = 'Swapcard API request executed and queues collected. Proceeding with local CRUD operations.';

  /**
    * Graphql requires "aliases" on (some) fields.
    *
    * @var array
    */
  const FIELD_ALIASES = [
    'integer' => 'value',
  ];

  /**
   * Parse retrieved data to make it ready for node entity create array.
   *
   * @param array $data
   *   Associative array with values retrieved from Swapcard API.
   * @param array $fields
   *   Associative array with fields mapped in this module's config settings.
   * @param Drupal\node\NodeInterface $node
   *   Existing node entity object or null.
   *
   * @return array
   *   An array with values ready for Node::create method.
   */
  public function prepareItem(array $data, array $fields, NodeInterface $node = NULL);

  /**
   * Either create or update node.
   *
   * @param array $data
   *   Associative array with values retrieved from Swapcard API.
   * @param array $node_data
   *   Node data populated, ready for Node::create() method.
   * @param Drupal\node\NodeInterface $node
   *   Existing node entity object or null.
   *
   * @return mixed
   *   ID of just saved node or false.
   */
  public function saveItem(array $data, array $node_data, NodeInterface $node = NULL);

}
