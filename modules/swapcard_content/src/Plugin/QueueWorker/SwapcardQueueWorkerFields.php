<?php

namespace Drupal\swapcard_content\Plugin\QueueWorker;

use Drupal\field\Entity\FieldStorageConfig;
use Symfony\Component\Yaml\Yaml;

/**
 * Creates QueueWorker for Swapcard fields.
 *
 * @QueueWorker(
 *   id = "swapcard_content_queue_swapcard_fields",
 *   title = @Translation("Swapcard Fields"),
 *   cron = {"time" = 90}
 * )
 */
class SwapcardQueueWorkerFields extends SwapcardQueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {

    if (!is_array($data) || empty($data)) {
      return;
    }

    foreach ($data as $field_id => $field_storage) {
      $options[$field_id] = $this->processFieldData($field_storage);
    }
    return $options;
  }

  /**
   * Generate field's allowed_values array.
   *
   * @param Drupal\field\Entity\FieldStorageConfig $definition
   *   Field config definition.
   * @param array $events
   *   An array with events ids.
   * @param bool $batch
   *   A flag for to determine if this is batch from UI or Cron run.
   *
   * @return array
   *   Associative array with key|Label pairs for select list type of field.
   */
  public function processFieldData(FieldStorageConfig $definition, array $events = [], bool $batch = FALSE) {

    $parent_config = $this->configFactory->get('swapcard.settings');
    $guzzle_options = $parent_config->get('guzzle_options');

    if ($parent_config->get('api_key') && !empty($guzzle_options['base_uri'])) {

      $events = !empty($events) ? $events : $this->config->get('events');

      // Note that Swapcard field's options (key|value)
      // are the same across all events. Luckily!
      if (!empty($events) && isset($events[0]) && !empty($events[0])) {

        $field_id = $definition->id();
        $field_name = $definition->getName();

        $options = [];

        // Check on defualt fields first.
        foreach ($this->config->get('node_types') as $fields) {
          if (in_array($field_name, array_keys($fields))) {

            $field = $fields[$field_name];
            $field['events'] = $events;

            foreach ($events as $event_id) {

              $cid = 'swapcard_content:' . $event_id . ':' . $field_id;

              // Returned cached data. Especially worth when we speak about
              // node_edit forms where this request would run every time.
              if ($cached = $this->cache->get($cid)) {
                if (!empty($cached->data)) {
                  return $cached->data;
                }
              }

              // Run fields definition request for the event.
              $options[$field_id] = $this->fetchFieldData($event_id, $field, 'event', $batch);

              // Set this to cache.
              if (!empty($options[$field_id])) {
                $cache_tags = [$cid];
                $this->cache->set($cid, $options, $this->cache::CACHE_PERMANENT, $cache_tags);
                return $options;
              }
            }
          }
        }
        // Return array now, ready for list string like options.
        return $options;
      }
      // Events are not defined on config form, warn about it.
      else {
        $this->apiStatus('warning', 'events');
      }
    }
    // API key is missing on config form, warn about it.
    else {
      $this->apiStatus('error', 'api');
    }

  }

  /**
   * Process fields.
   *
   * @todo this one.
   *
   * @param array $default_fields
   *   An array of default fields.
   * @param array $filtered_fields
   *   An array of custom fields.
   * @param array $node_types
   *   Default config's node_types array.
   *
   * @return array
   *   An array containing fields to be updated.
   */
  public function processFields(array $default_fields, array $filtered_fields, array $node_types) {

    $plugin = $this->queueManager->createInstance('swapcard_content_queue_swapcard_event');
    $field_ids = [];
    $update_fields = [];
    $existing_fields = [];

    foreach ($node_types as $bundle => $fields) {

      $update_fields[$bundle] = $fields;

      if (!empty($default_fields)) {
        foreach ($default_fields as $field_name => $field_value) {
          if (isset($fields[$field_name])) {
            if (!empty($field_value)) {
              // Here we specifically update main config/settings with injecting
              // "id" value to each of the standard fields of a kind.
              $update_fields[$bundle][$field_name]['id'] = $field_value;

              $field_id = 'node.' . $bundle . '.' . $field_name;
              $field_config = $this->entityTypeManager->getStorage('field_config')->load($field_id);
              $field_ids[$field_id] = $field_config->getFieldStorageDefinition();
            }
            else {
              $update_fields[$bundle][$field_name]['id'] = NULL;
            }
          }
        }
        // Take care of custom fields now.
        foreach ($fields as $drupal_field_name => $swapcard_field) {
          if (isset($swapcard_field['custom']) && isset($swapcard_field['id'])) {
            $field_id = 'node.' . $bundle . '.' . $drupal_field_name;
            $existing_fields[$field_id] = $swapcard_field['id'];
          }
        }
      }

      // REVERSE check on previously exitsing (but not anymore) fields.
      $current_fields = [];
      if (!empty($filtered_fields)) {
        foreach ($filtered_fields as $filtered_field) {
          if (isset($filtered_field['drupal_field_name']) && isset($filtered_field['swapcard_field_id'])) {
            $current_fields[$filtered_field['drupal_field_name']] = $filtered_field['swapcard_field_id'];
          }
        }
      }

      $diff = array_diff(array_keys($existing_fields), array_keys($current_fields));

      if (!empty($diff)) {
        foreach ($diff as $diff_field_id) {
          $field_name_array = strpos($diff_field_id, '.') !== FALSE ? explode('.', $diff_field_id) : [];
          $drupal_field_name = count($field_name_array) > 2 ? array_pop($field_name_array) : NULL;
          $field_bundle = count($field_name_array) > 1 ? array_pop($field_name_array) : NULL;
          if ($drupal_field_name && $field_bundle) {
            if (isset($fields[$drupal_field_name]['custom']) && isset($fields[$drupal_field_name]['id'])) {
              unset($update_fields[$field_bundle][$drupal_field_name]);
            }
          }
        }
      }

      if (!empty($filtered_fields)) {

        foreach ($filtered_fields as $filtered_field) {

          if (isset($filtered_field['drupal_field_name']) && !empty($filtered_field['drupal_field_name']) && isset($filtered_field['swapcard_field_id']) && !empty($filtered_field['swapcard_field_id'])) {

            $field_name_array = strpos($filtered_field['drupal_field_name'], '.') !== FALSE ? explode('.', $filtered_field['drupal_field_name']) : [];
            $drupal_field_name = count($field_name_array) > 2 ? array_pop($field_name_array) : NULL;
            $field_bundle = count($field_name_array) > 1 ? array_pop($field_name_array) : NULL;

            if ($drupal_field_name && $field_bundle) {

              $exists = in_array($drupal_field_name, array_keys($fields));
              $is_custom = isset($fields[$drupal_field_name]['custom']) && isset($fields[$drupal_field_name]['id']);

              if ($exists) {
                if (!$is_custom) {
                  $error = $this->t('Field @field_name cannot be defined as custom, it belongs to an array of default included fields.', [
                    '@field_name' => $drupal_field_name,
                  ]);
                  $plugin->messenger->addError($error);
                }
                else {
                  $this->processField($filtered_field, $update_fields, $field_ids);
                }
              }
              else {
                $this->processField($filtered_field, $update_fields, $field_ids);
              }
            }
          }
        }
      }
    }

    return [
      'update_fields' => $update_fields,
      'field_ids' => $field_ids,
    ];
  }

  /**
   * Add field's id or a complete custom field to config.
   *
   * @param array $custom_field
   *   An array with custom field's properties.
   * @param array $update_fields
   *   Reference array containing update data for node_types config property.
   * @param array $field_ids
   *   Reference array of fields/definitions to refresh allowed_values.
   */
  private function processField(array $custom_field, array &$update_fields, array &$field_ids) {

    if ($field_config = $this->entityTypeManager->getStorage('field_config')->load($custom_field['drupal_field_name'])) {

      $target_bundle = $field_config->getTargetBundle();
      $field_name = $field_config->getName();

      $update_fields[$field_config->getTargetBundle()][$field_config->getName()] = [
        'id' => $custom_field['swapcard_field_id'],
        'origin' => 'fields',
        'type' => $field_config->getType(),
        'custom' => TRUE,
      ];
      $texts = ['text', 'text_long', 'text_with_summary'];

      if (in_array($field_config->getType(), $texts)) {
        $update_fields[$target_bundle][$field_name]['format_name'] = 'basic_html';
      }
      $field_ids[$custom_field['drupal_field_name']] = $field_config->getFieldStorageDefinition();

      // Automatically set and save "allowed_values_function"
      // for any List (string) type of fields.
      if ($field_config->getType() == 'list_string') {

        $settings = $field_config->getSettings();

        if (!isset($settings['allowed_values_function']) || (isset($settings['allowed_values_function']) && empty($settings['allowed_values_function']))) {
          $field_storage_id = 'field.storage.' . $field_config->getTargetEntityTypeId() . '.' . $field_name;
          $field_storage = $this->configFactory->getEditable($field_storage_id);
          $settings['allowed_values'] = [];
          $settings['allowed_values_function'] = '_swapcard_content_fields_options';
          if ($field_storage->set('settings', $settings)) {
            $field_storage->save();
          }
        }
      }
    }
  }

  /**
   * Get fields definitions from API.
   *
   * @param string $event_id
   *   Id of the event being processed.
   * @param array $field
   *   An array of values related to a field in drupal we match we remote.
   * @param string $swapcard_callback
   *   Swapcard API callback function name.
   * @param bool $batch
   *   A flag for to determine if this is batch from UI or Cron run.
   *
   * @return array
   *   Associative array with key|Label pairs for select list type of field.
   */
  protected function fetchFieldData(string $event_id, array $field, string $swapcard_callback, bool $batch = FALSE) {

    $parent_config = $this->configFactory->get('swapcard.settings');
    $guzzle_options = $parent_config->get('guzzle_options');

    $options = [];

    // TEST - add test response array to the queue.
    // Comes from YML file, instad of calling a remote API.
    if ($this->config->get('test') || $this->testPath) {

      $test_file_path = static::DUMP_PATH . '/swapcard_fields_' . $event_id . '_' . $field['id'] . '.test.yml';
      if (file_exists($test_file_path)) {
        $response['data'][$swapcard_callback] = Yaml::parseFile($test_file_path);
      }
      else {
        $this->messenger->addError($this->t('Specified test file <em>@path</em> does not exist.', [
          '@path' => $test_file_path,
        ]));
        return [];
      }
    }
    else {

      $parent_config = $this->configFactory->get('swapcard.settings');
      $guzzle_options = $parent_config->get('guzzle_options');

      $swapcard_plugin_manager = $this->swapcardPluginManager->createInstance('swapcard_fields', ['all']);
      $args = [
        'id' => '\"' . $event_id . '\"',
      ];

      // VERY ugly, but for now we do not have possibility to put suuch params
      // in annotation of Swapcard plugin.
      $query_string = $swapcard_plugin_manager->queryString($swapcard_callback, $args);
      $custom_query_string = 'fieldDefinitions(ids: ' . '\"' . $field['id'] . '\")';

      $callback_options = [
        'body' => str_replace('fieldDefinitions', $custom_query_string, $query_string),
      ];

      $response = $swapcard_plugin_manager->post($guzzle_options['base_uri'], $callback_options);
    }

    if (is_array($response) && isset($response['data']) && count($response['data']) > 0 && isset($response['data'][$swapcard_callback])) {

      // DUMP TEST file, if that's set on config.
      if ($this->config->get('dump')) {
        $this->dumpResponse($response['data'][$swapcard_callback], 'swapcard_fields_' . $event_id . '_' . $field['id'] . '.test.yml');
      }

      foreach ($response['data'] as $response_data) {
        if (isset($response_data['fieldDefinitions']) && !empty($response_data['fieldDefinitions'])) {

          foreach ($response_data['fieldDefinitions'] as $field_definition) {

            if (isset($field_definition['id']) && $field_definition['id'] == $field['id']) {

              switch ($field['type']) {

                case 'list_string':

                  if (isset($field_definition['optionsValues']) && !empty($field_definition['optionsValues'])) {

                    foreach ($field_definition['optionsValues'] as $optionsValue) {
                      if (isset($optionsValue['value']) && !empty($optionsValue['value'])) {

                        $options[$optionsValue['value']] = isset($optionsValue['translations']) && !empty($optionsValue['translations']) && isset($optionsValue['translations'][0]['value']) ? $optionsValue['translations'][0]['value'] : $optionsValue['value'];
                      }
                    }
                  }
                  break;
              }
            }
          }
        }
      }
    }

    return $options;
  }

}
