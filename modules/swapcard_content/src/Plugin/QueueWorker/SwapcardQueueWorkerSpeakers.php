<?php

namespace Drupal\swapcard_content\Plugin\QueueWorker;

use Drupal\node\NodeInterface;

/**
 * Creates or updates nodes based on results retrived by API call/response.
 *
 * @QueueWorker(
 *   id = "swapcard_content_queue_swapcard_speaker",
 *   title = @Translation("Swapcard Speakers"),
 *   cron = {"time" = 120}
 * )
 */
class SwapcardQueueWorkerSpeakers extends SwapcardQueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function prepareItem(array $data, array $fields, NodeInterface $node = NULL) {

    // Invoke parent method.
    // This prepares a lot of default fields, hence here only specifics.
    $node_data = parent::prepareItem($data, $fields);

    foreach ($fields as $drupal_field_name => $swapcard_field) {

      switch ($swapcard_field['type']) {

        // A special care for Social networks property.
        case 'link':
          if (isset($swapcard_field['origin']) && isset($data[$swapcard_field['origin']]) && isset($swapcard_field['options'])) {

            foreach ($data[$swapcard_field['origin']] as $delta => $provider) {
              $type = isset($provider['type']) && isset($provider['profile']) ? strtolower($provider['type']) : NULL;
              if ($type && in_array($type, array_keys($swapcard_field['options']))) {

                $prefix = strpos($swapcard_field['options'][$type], '//') !== FALSE ? '' : 'https://';
                $suffix = strpos($swapcard_field['options'][$type], '//') !== FALSE ? '' : '/';
                $url = $prefix . $swapcard_field['options'][$type] . $suffix . $provider['profile'];

                $node_data[$drupal_field_name][$delta] = [
                  'uri' => $url,
                  'title' => ucfirst($type),
                ];
              }
              else {
                $node_data[$drupal_field_name][$delta] = NULL;
              }
            }
          }
          else {
            $node_data[$drupal_field_name] = NULL;
          }
          break;

        // We have pretty unique case here with Speaker's ID.
        case 'string':
          if (isset($swapcard_field['unique'])) {
            $sessions_manager = $this->queueManager->createInstance('swapcard_content_queue_swapcard_session');
            if ($speaker_id = $sessions_manager->matchSpeakers($data)) {

              $node_data[$drupal_field_name] = $speaker_id;

              $existing_speaker_properties = [
                'type' => 'swapcard_speaker',
                'field_swapcard_speaker_id' => $speaker_id,
              ];
              $existing_speaker = $this->existingEntity('node', $existing_speaker_properties);

              if ($existing_speaker instanceof NodeInterface) {
                $node_data['existing_entity'] = $existing_speaker;
              }
            }
            else {
              $node_data[$drupal_field_name] = NULL;
            }
          }
          break;

        case 'base_array':

          if (isset($swapcard_field['origin'])) {
            $title_array = $this->processTitle($swapcard_field['origin'], $data);
            $node_data[$drupal_field_name] = !empty($title_array) ? implode(' ', $title_array) : static::PLACEHOLDER_TITLE;
          }
          else {
            $node_data[$drupal_field_name] = NULL;
          }

          break;
      }
    }

    return $node_data;
  }

}
