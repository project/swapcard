<?php

namespace Drupal\swapcard_content\Plugin\QueueWorker;

use Drupal\Core\Batch\BatchBuilder;

/**
 * Creates QueueWorker for a data request.
 *
 * @QueueWorker(
 *   id = "swapcard_content_queue_swapcard_request",
 *   title = @Translation("Swapcard API Request"),
 *   cron = {"time" = 40}
 * )
 */
class SwapcardQueueWorkerRequest extends SwapcardQueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {

    if (!isset($data['batch'])) {
      return;
    }

    $response = [];

    // First take care of any call from "external", some ther module or so.
    if (isset($data['hook']) && !empty($data['hook']) && isset($data['hook']['name']) && isset($data['hook']['callback'])) {
      $plugin = $this->queueManager->createInstance($data['hook']['name']);
      $response = $plugin->processRequest($data['hook']['name'], $data['hook']['callback'], $data['hook']['callback'], $data);
    }
    else {
      $entities = $this->checkEventEntities();

      // This is Events only.
      if (empty($entities)) {
        $plugin = $this->queueManager->createInstance('swapcard_content_queue_swapcard_event');
        $response = $plugin->processRequest('swapcard_content_queue_swapcard_event', 'swapcard_event', 'events', $data);
      }
      // Some entities such as Exhibitors or Sessions are selected.
      else {
        if (isset($entities['entity_key']) && isset($entities['swapcard_callback'])) {
          $plugin = $this->queueManager->createInstance('swapcard_content_queue_' . $entities['entity_key']);
          $response = $plugin->processRequest('swapcard_content_queue_swapcard_event', $entities['entity_key'], $entities['swapcard_callback'], $data);
        }
      }
    }

    // Assign new events if specified as arg and not exitsing in config before,
    // most likely this is invoked from a drush command.
    if (isset($data['events']) && !empty($data['events'])) {
      if ($config = $this->configFactory->getEditable('swapcard_content.settings')) {
        $default_events = $config->get('events') && !empty($config->get('events')) ? $config->get('events') : [];
        $filtered_events = array_filter($data['events'], function ($value) use ($default_events) {
          return !in_array($value, array_values($default_events));
        });
        if (!empty($filtered_events)) {
          $set_events = empty($default_events) ? $filtered_events : array_merge($default_events, $filtered_events);
          $config->set('events', $set_events);
          $config->save();
        }
      }
    }

    // Add the other modules ability to make some custom logic or IA changes.
    $this->moduleHandler->alter('swapcard_content_response', $data, $response);

    // Execution is by cron. Use "--quiet' option when running with drush cron
    // and wanting to suppress progress info in console.
    if (!empty($response) && $data['batch'] == FALSE) {

      // Define batch builder.
      $cron_batch_builder = new BatchBuilder();

      $default_fields = [];
      $custom_fields = [];
      $node_types = $this->config->get('node_types');
      if (!empty($node_types)) {
        foreach ($node_types as $fields) {
          foreach ($fields as $drupal_field_name => $field) {
            if (isset($field['origin']) && $field['origin'] == 'fields' && isset($field['id']) && !empty($field['id'])) {
              if (isset($field['custom']) && $field['custom']) {
                $custom_fields[$drupal_field_name] = $field;
              }
              else {
                $default_fields[$drupal_field_name] = $field;
              }
            }
          }
        }

        // Add fields as a first request/batch.
        $fields_plugin = $this->queueManager->createInstance('swapcard_content_queue_swapcard_fields');
        $update_fields = $fields_plugin->processFields($default_fields, $custom_fields, $node_types);
        if (isset($update_fields['field_ids']) && !empty($update_fields['field_ids'])) {
          $fields_plugin->queueFactory->get('swapcard_content_queue_swapcard_fields')->createItem($update_fields['field_ids']);
          array_unshift($response, 'swapcard_content_queue_swapcard_fields');
        }
      }

      // Add all the other batches.
      foreach ($response as $b) {
        $cron_batch_builder->addOperation([$this->queueUi, 'step'], [$b]);
      }

      // Set and configure the batch.
      batch_set($cron_batch_builder->toArray());
      $cron_batch = & batch_get();
      $cron_batch['progressive'] = TRUE;

      // Process the batch.
      drush_backend_batch_process();

    }
    return $response;

  }

}
