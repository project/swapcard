<?php

namespace Drupal\swapcard_content\Plugin\QueueWorker;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Queue\QueueWorkerManager;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\filter\FilterFormatInterface;
use Drupal\node\NodeInterface;
use Drupal\queue_ui\QueueUIBatchInterface;
use Drupal\swapcard\Plugin\SwapcardPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Creates or updates nodes based on results retrived by API call/response.
 */
abstract class SwapcardQueueWorkerBase extends QueueWorkerBase implements ContainerFactoryPluginInterface, SwapcardQueueWorkerInterface {

  /**
   * Drupal\Core\StringTranslation\StringTranslationTrait definition.
   *
   * Wrapper methods for \Drupal\Core\StringTranslation\TranslationInterface.
   *
   * @var \Drupal\Core\StringTranslation\StringTranslationTrait
   */
  use StringTranslationTrait;

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  public $messenger;

  /**
   * Drupal\Core\Queue\QueueWorkerManager definition.
   *
   * @var \Drupal\Core\Queue\QueueWorkerManager
   */
  public $queueManager;

  /**
   * Drupal\Core\Queue\QueueFactory definition.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  public $queueFactory;

  /**
   * Drupal\queue_ui\QueueUIBatchInterface definition.
   *
   * @var \Drupal\queue_ui\QueueUIBatchInterface
   */
  public $queueUi;

  /**
   * The State interface.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  public $state;

  /**
   * Local debug data path (from yml).
   *
   * @var string
   */
  protected $testPath;

  /**
   * Events, first API response, container.
   *
   * @var array
   */
  protected $author;

  /**
   * Drupal\Core\Database\Connection class.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * This module's config object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  public $config;

  /**
   * This Swapcard module's config object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $parentConfig;

  /**
   * Drupal\swapcard\Plugin\SwapcardPluginInterface definition.
   *
   * @var \Drupal\swapcard\Plugin\SwapcardPluginInterface
   */
  protected $swapcardPluginManager;

  /**
   * Drupal\Core\Cache\CacheBackendInterface definition.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  public $configFactory;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Entity\EntityFieldManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Drupal\Core\File\FileSystemInterface definition.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Drupal\Core\Logger\LoggerChannelFactoryInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  public $logger;

  /**
   * Drupal\Core\Extension\ModuleHandlerInterface definition.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  public $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    Connection $database,
    CacheBackendInterface $cache,
    ConfigFactoryInterface $config_factory,
    SwapcardPluginManager $swapcard_plugin_manager,
    QueueWorkerManager $queue_manager,
    QueueFactory $queue_factory,
    QueueUIBatchInterface $queue_ui,
    StateInterface $state,
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager,
    FileSystemInterface $file_system,
    LoggerChannelFactoryInterface $logger,
    MessengerInterface $messenger,
    ModuleHandlerInterface $module_handler) {

    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->configFactory = $config_factory;
    $this->parentConfig = $this->configFactory->get('swapcard.settings');
    $this->config = $this->configFactory->get('swapcard_content.settings');
    $this->author = $this->config->get('default_uid');
    $this->database = $database;
    $this->cache = $cache;
    $this->swapcardPluginManager = $swapcard_plugin_manager;
    $this->queueManager = $queue_manager;
    $this->queueFactory = $queue_factory;
    $this->queueUi = $queue_ui;
    $this->state = $state;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->fileSystem = $file_system;
    $this->logger = $logger;
    $this->messenger = $messenger;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('database'),
      $container->get('cache.default'),
      $container->get('config.factory'),
      $container->get('plugin.manager.swapcard'),
      $container->get('plugin.manager.queue_worker'),
      $container->get('queue'),
      $container->get('queue_ui.batch'),
      $container->get('state'),
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('file_system'),
      $container->get('logger.factory'),
      $container->get('messenger'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {

    if (!is_array($data) || empty($data)) {
      return;
    }

    $nodes = [];
    if (!in_array('id', array_keys($data))) {

      foreach ($data as $item) {
        // Load fields we want to process.
        $fields = $this->config->get('node_types')[$item['node_type']];
        if (!empty($fields)) {

          // Prepare our data for to be associative array ready for node save.
          $node_data = $this->prepareItem($item, $fields);

          // Check for existing entity (node).
          $existing_entity = NULL;
          if (isset($node_data['existing_entity']) || is_null($node_data['existing_entity'])) {
            $existing_entity = $node_data['existing_entity'] instanceof NodeInterface ? $node_data['existing_entity'] : NULL;
            // Essential.
            unset($node_data['existing_entity']);
          }
          // Now run "save" callback  to actually save this entity.
          if ($saved = $this->saveItem($item, $node_data, $existing_entity)) {
            $saved->data = $item;
            $nodes[$saved->id()] = $saved;
          }
        }
      }
      if (!empty($nodes)) {
        // Add the other modules ability to make custom logic or IA changes.
        $this->moduleHandler->alter('swapcard_content_post_save', $data, $nodes);
      }
    }
    else {
      // $count = 1;
      // $index = 1;
      // Load fields we want to process.
      $fields = $this->config->get('node_types')[$data['node_type']];
      if (!empty($fields)) {
        // Prepare our data for to be associative array ready for node save.
        $node_data = $this->prepareItem($data, $fields);

        // Check for existing entity (node).
        $existing_entity = NULL;
        if (isset($node_data['existing_entity']) || is_null($node_data['existing_entity'])) {
          $existing_entity = $node_data['existing_entity'] instanceof NodeInterface ?? $node_data['existing_entity'];
          // Essential.
          unset($node_data['existing_entity']);
        }
        // Now run "save" callback  to actually save this entity.
        if ($saved = $this->saveItem($data, $node_data, $existing_entity)) {
          $saved->data = $data;
          $nodes[$saved->id()] = $saved;
          // Add the other modules ability to make custom logic or IA changes.
          $this->moduleHandler->alter('swapcard_content_post_save', $data, $nodes);
        }
      }
    }
  }

  /**
   * Set testPath property to use test array (from yml).
   *
   * @param array $raw_data
   *   A data from API response in "original" state".
   * @param string $test_file_name
   *   File name of a test YML file.
   */
  public function dumpResponse(array $raw_data, string $test_file_name) {
    $destination = static::DUMP_PATH;
    if ($this->fileSystem->prepareDirectory($destination, FileSystemInterface::CREATE_DIRECTORY)) {
      if (\Drupal::service('file.repository')->writeData(Yaml::dump($raw_data), $destination . '/' . $test_file_name, FileSystemInterface::EXISTS_REPLACE)) {
        $status = $this->t('Dumped file for testing: <em>@file</em>', ['@file' => $test_file_name]);
        $this->messenger->addStatus($status);
      }
    }
  }

  /**
   * Check for Event's entities selected on config.
   *
   * @param bool $sessions
   *   If set this is exclusive for sessions.
   *
   * @return array
   *   Associative array with info about Event's entities in the game.
   */
  public function checkEventEntities(bool $sessions = FALSE) {

    $entities = $this->config->get('entities');
    $keys = [];

    if (!$sessions && isset($entities['swapcard_exhibitor'])) {
      $keys['entity_key'] = 'swapcard_exhibitor';
      $keys['swapcard_callback'] = 'exhibitors';
      if (isset($entities['swapcard_session'])) {
        $keys['sessions_key'] = 'swapcard_session';
        if (isset($entities['swapcard_speaker'])) {
          $keys['speakers_key'] = 'swapcard_speaker';
        }
      }
    }
    else {
      if (isset($entities['swapcard_session'])) {
        $keys['entity_key'] = 'swapcard_session';
        $keys['sessions_key'] = 'swapcard_session';
        $keys['swapcard_callback'] = 'plannings';
        if (isset($entities['swapcard_speaker'])) {
          $keys['speakers_key'] = 'swapcard_speaker';
        }
      }
    }
    return $keys;
  }

  /**
   * Check for presence of Swapcard content media.
   *
   * @param array $data
   *   Associative array with properties from any entity calling this method.
   * @param string $node_type
   *   If set this is exclusive for that node type.
   *
   * @return array
   *   Associative array with Media queue name and API origin property.
   */
  public function hasMedia(array $data, string $node_type = NULL) {
    $definitions = $this->queueManager->getDefinitions();
    if (is_array($definitions) && in_array('swapcard_content_queue_swapcard_media', array_keys($definitions))) {
      $type = $node_type ? $node_type : $data['node_type'];
      $origin = isset($this->configFactory->get('swapcard_content_media.settings')->get('node_types')[$type]) ?? NULL;
      return [
        'queue_name' => 'swapcard_content_queue_swapcard_media',
      // $this->configFactory->get('swapcard_content_media.settings')->get('node_types')[$type]['origin'],
        'origin' => $origin,
      ];
    }
    return [];
  }

  /**
   * Common status messages and logs.
   *
   * @param string $status
   *   A type of the message, i.e. status, warning, error.
   * @param string $type
   *   Type of data/field.
   */
  public function apiStatus(string $status, string $type) {

    $message = NULL;
    $addStatus = 'add' . ucfirst($status);

    if ($type == 'api') {
      $message = $this->t('Swapcard API key and/or base url not set, make sure you do so on <a href="@url">Swapcard config form!</a>', [
        '@url' => '/admin/config/services/swapcard/config',
      ]);
    }
    elseif ($type == 'events') {
      $message = $this->t('There is no Swapcard Events defined in configuration, make sure you do so on <a href="@url">Swapcard config form!</a>', [
        '@url' => '/admin/config/services/swapcard/config',
      ]);
    }

    if ($message) {
      // Set in watchdog log in any case.
      $this->logger->get('swapcard_content')->{$status}($message);
      // Show in UI if cron is not run from shell. As a Drupal message.
      if (PHP_SAPI !== 'cli') {
        $this->messenger->{$addStatus}($message);
      }
    }
  }

  /**
   * Notifications in shell.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Content entity object, either node or paragraph for now.
   * @param string $op
   *   Operation for which is notyfing.
   * @param string $status
   *   Optional prepared status string to use for this notification.
   * @param \Drupal\Core\Entity\ContentEntityInterface $subentity
   *   Content entity object, can be paragraph
   *   in case when $entity is node object.
   */
  public function notify(ContentEntityInterface $entity, string $op = 'Created', string $status = NULL, ContentEntityInterface $subentity = NULL) {
    if (PHP_SAPI === 'cli' && $this->config->get('cron_verbose')) {
      $params = [
        '@op' => $op,
        '@type' => $entity->bundle(),
        '@entity_type' => ucfirst($entity->getEntityTypeId()),
        '@title' => $entity->label(),
      ];

      if ($subentity) {
        $params['@subentity'] = $subentity->label();
        $status = $status ? $status : $this->t('@op @entity_type type @type with title "@title" with @subentity entity.', $params);
      }
      else {
        $status = $status ? $status : $this->t('@op @entity_type type @type with title "@title".', $params);
      }
      print $status . "\r\n";
    }
  }

  /**
   * Main request operation.
   *
   * This method takes into account the various dependencies between sessions,
   * speakers, and exhibitors. There is logic is this method, for example, to
   * run exhibitors (if selected) before sessions (as exhibitors can reference
   * sessions).
   *
   * @param string $queue_name
   *   A queue plugin name.
   * @param string $node_type
   *   Content type we are reating/updating in this round.
   *   At the same time it is also "Swapcard" plugin id.
   * @param string $swapcard_callback
   *   Swapcard API callback function name.
   * @param array $data
   *   A set of initial params sent here, such as boolean batch etc.
   *
   * @return array
   *   Associative array with all the requested data from API.
   */
  public function processRequest(string $queue_name, string $node_type, string $swapcard_callback, array $data) {

    $batch = $data['batch'];

    $parent_config = $this->configFactory->get('swapcard.settings');
    $guzzle_options = $parent_config->get('guzzle_options');

    // We do not want anything to run
    // if credentials were not stored in configuration.
    if ($parent_config->get('api_key') && !empty($guzzle_options['base_uri'])) {

      $config_events = isset($data['events']) && !empty($data['events']) ? $data['events'] : $this->config->get('events');
      $entities = $this->checkEventEntities();

      // Events are not entered in config form, warn about it and return.
      if (empty($config_events)) {
        $this->apiStatus('warning', 'events');
        return [];
      }
      else {
        $events = array_combine($config_events, $config_events);
      }

      $exhibitor_data = [];
      $sessions_data = [];
      $batches = [];

      // No event content (sessions, exhibitors) has been selected for syncing
      // on the config page.
      if (empty($entities) || ($node_type != 'swapcard_session' && $node_type != 'swapcard_exhibitor')) {

        $events_response = $this->processEvents($events, 'swapcard_event', $batch, $batches);
        if (!empty($events_response)) {

          $this->queueFactory->get('swapcard_content_queue_swapcard_event')->createItem($events_response);

          if (!in_array('swapcard_content_queue_swapcard_event', $batches)) {
            $batches[] = 'swapcard_content_queue_swapcard_event';
          }
          if ($batch) {
            $this->queueUi->batch($batches);
            return $events_response;
          }
          else {
            return $batches;
          }
        }
      }
      else {

        if (!in_array('swapcard_content_queue_swapcard_event', $batches)) {
          $batches[] = 'swapcard_content_queue_swapcard_event';
        }

        if (($entities['entity_key'] == 'swapcard_session' || isset($entities['sessions_key'])) && isset($entities['speakers_key'])) {
          if (!in_array('swapcard_content_queue_swapcard_speaker', $batches)) {
            $batches[] = 'swapcard_content_queue_swapcard_speaker';
          }
        }

        $exhibitor_data = $this->processEventEntities($events, $node_type, $swapcard_callback, $batch, $batches);

        if (!empty($exhibitor_data)) {

          // Add data to queue.
          if (isset($entities['entity_key']) && $this->queueFactory->get('swapcard_content_queue_' . $entities['entity_key'])) {

            if ($entities['entity_key'] == 'swapcard_exhibitor' && $node_type == 'swapcard_exhibitor') {

              // Deal with Sessions, which may be selected
              // along with exhibitors.
              if (isset($entities['sessions_key'])) {

                $sessions_data = $this->processEventEntities($exhibitor_data, 'swapcard_session', 'plannings', $batch, $batches);

                if (!empty($sessions_data)) {
                  $this->processExhibitors($sessions_data, $batch, $batches);
                  $this->processSessions($sessions_data, $batch, $batches);
                }
                else {
                  $this->processExhibitors($exhibitor_data, $batch, $batches);
                }
              }
              else {
                $this->processExhibitors($exhibitor_data, $batch, $batches);
              }
            }
            elseif ($entities['entity_key'] == 'swapcard_session' && $node_type == 'swapcard_session') {
              $this->processSessions($exhibitor_data, $batch, $batches);
            }
          }
        }

        // There was no (0) Exhibitors on this Event.
        else {

          // Check if any sessions then.
          $sessions_data = $this->processEventEntities($events, 'swapcard_session', 'plannings', $batch, $batches);

          if (!empty($sessions_data)) {
            $this->processSessions($sessions_data, $batch, $batches);
          }
          else {
            $this->processEvents($events, 'swapcard_event', $batch, $batches);
          }
        }

        $batches_data = !empty($sessions_data) ? $sessions_data : $exhibitor_data;
        if (!$batches_data) {
          $batches_data = [];
        }

        // Add the other modules ability to extend batches before final run.
        $this->moduleHandler->alter('swapcard_batches', $batches, $batches_data);

        // Set batches and have them execute,one after another.
        if ($batch && !empty($batches)) {

          // If there are any entities flagged for deleting then this will
          // process it.
          $batches[] = 'swapcard_content_queue_swapcard_delete';

          // This is ONLY for the more accurate info/message,
          // that displays after all of the batches have finished.
          $batches[] = 'swapcard_content_queue_swapcard_entities';

          // Finally, run QueueUI batch.
          $this->queueUi->batch($batches);
        }
        else {

          if (!empty($batches)) {
            // If there are any entities flagged for deleting then this will
            // process it.
            $batches[] = 'swapcard_content_queue_swapcard_delete';
          }

          return $batches;
        }
      }
      return $batches_data;
    }
    else {
      $this->apiStatus('error', 'api');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function prepareItem(array $data, array $fields, NodeInterface $node = NULL) {

    $existing_entity_properties = [];
    $existing_entity_properties['field_' . $data['node_type'] . '_id'] = $data['id'];
    $entity_type_id = isset($data['entity_type_id']) && !empty($data['entity_type_id']) ? $data['entity_type_id'] : 'node';

    $existing_entity = $this->existingEntity($entity_type_id, $existing_entity_properties);

    // Base, mandatory node data.
    $node_data = [
      'type' => $data['node_type'],
      'uid' => $this->author,
      'existing_entity' => $existing_entity,
    ];

    // Add the other modules ability to make some custom logic or IA changes.
    $this->moduleHandler->alter('swapcard_content_prepare', $data, $node_data, $fields, $existing_entity);

    $matched_fields = [];

    foreach ($fields as $drupal_field_name => $swapcard_field) {

      if (isset($swapcard_field['origin'])) {

        $property_exists = $swapcard_field['type'] == 'entity_reference' || ($swapcard_field['origin'] == 'fields' && isset($data[$swapcard_field['origin']]) && !empty($data[$swapcard_field['origin']]));

        if ($property_exists) {
          // Those are fields matched by its ID.
          $data += $this->processById($data, $swapcard_field, $drupal_field_name);
          $swapcard_field['origin'] = $swapcard_field['id'];
          $matched_fields[$drupal_field_name] = $swapcard_field;

        }
      }
    }

    if (!empty($matched_fields)) {
      $fields = array_merge($fields, $matched_fields);
    }

    foreach ($fields as $drupal_field_name => $swapcard_field) {

      $value = NULL;

      $property = $swapcard_field['type'] == 'entity_reference' || isset($swapcard_field['origin']) ? $swapcard_field['origin'] : NULL;

      if (is_array($property)) {
        if (!empty($property)) {
          foreach ($property as $property_key) {
            if (isset($data[$property_key]) && !empty($data[$property_key])) {
              $value[$property_key] = $data[$property_key];
            }
          }
        }
      }
      else {
        if (!$property && $swapcard_field['type'] == 'entity_reference') {
          $value = isset($data[$drupal_field_name]) && !empty($data[$drupal_field_name]) ? $data[$drupal_field_name] : NULL;
        }
        else {
          $value = isset($data[$property]) && !empty($data[$property]) ? $data[$property] : NULL;
        }
      }

      if (!$value && $swapcard_field['type'] == 'base_field') {
        $value = static::PLACEHOLDER_TITLE;
      }

      if ($value) {

        switch ($swapcard_field['type']) {

          case 'boolean':
            $node_data[$drupal_field_name] = $value == NULL || $value == FALSE ? '0' : $value;
            break;

          case 'base_field':
          case 'integer':
          case 'decimal':
          case 'float':
          case 'string':
          case 'string_long':
          case 'list_string':
          case 'entity_reference':

            $node_data[$drupal_field_name] = $value;
            break;

          case 'entity_reference':
            $node_data[$drupal_field_name]['target_id'] = $value;
            break;

          case 'link':
            if (is_array($value)) {
              foreach ($value as $link) {
                if (is_string($link)) {
                  $node_data[$drupal_field_name]['uri'] = $link;
                }
              }
            }
            else {
              $node_data[$drupal_field_name]['uri'] = $value;
            }
            break;

          case 'text':
          case 'text_long':
          case 'text_with_summary':
            $format = NULL;
            if (isset($swapcard_field['format_name']) && !empty($swapcard_field['format_name'])) {
              $format_entity = $this->entityTypeManager->getStorage('filter_format')->load($swapcard_field['format_name']);
              if ($format_entity instanceof FilterFormatInterface && $format_entity->status() > 0) {
                $format = $format_entity->id();
              }
            }

            $node_data[$drupal_field_name] = [
              'value' => $value,
              'format' => $format ? $format : 'basic_html',
            ];

            break;

          case 'daterange':

            // Fetch default timezone from the main Drupal's
            // configuration at "/admin/config/regional/settings".
            $default_timezone = $this->configFactory->get('system.date')->get('timezone');
            $timezone = isset($data['timezone']) && !empty($data['timezone']) ? $data['timezone'] : $default_timezone['default'];

            // Property "beginsAt".
            $begins_at = is_array($value) && isset($value['beginsAt']) && !empty($value['beginsAt']) ? $value['beginsAt'] : NULL;
            // Property "endsAt".
            $ends_at = is_array($value) && isset($value['endsAt']) && !empty($value['endsAt']) ? $value['endsAt'] : NULL;

            if ($begins_at && $ends_at) {
              $node_data[$drupal_field_name] = $this->processDate($drupal_field_name, $data['node_type'], $begins_at, $ends_at, $timezone);
            }
            else {
              $node_data[$drupal_field_name] = NULL;
            }
            break;
        }
      }
      else {
        $node_data[$drupal_field_name] = $swapcard_field['type'] == 'boolean' ? '0' : NULL;
      }
    }

    // "Swapcard content media" plugin handling.
    $has_media = $this->hasMedia($data);

    if (isset($has_media['queue_name']) && isset($has_media['origin']) && $has_media['origin'] != FALSE) {
      $media_plugin = $this->queueManager->createInstance($has_media['queue_name']);
      $media_plugin->processMediaSource($data, $node_data, $existing_entity);
    }

    return $node_data;
  }

  /**
   * {@inheritdoc}
   */
  public function saveItem(array $data, array $node_data, NodeInterface $node = NULL) {

    try {

      $print_params = [
        '@title' => $node_data['title'],
        '@type' => $node_data['type'],
      ];

      if (!$node) {

        $existing_node_properties = [
          'type' => $node_data['type'],
        ];

        $existing_node_properties['field_' . $node_data['type'] . '_id'] = $data['id'];
        $node = $this->existingEntity('node', $existing_node_properties);
      }

      // This node exists, UPDATE with current values.
      if ($node instanceof NodeInterface) {
        $print_params['@op'] = 'Updated';

        foreach ($node_data as $node_field_name => $node_field_value) {
          $node->set($node_field_name, $node_field_value);
        }
      }
      // A brand NEW node.
      else {
        $print_params['@op'] = 'Created';
        $node = $this->entityTypeManager->getStorage('node')->create($node_data);
      }

      // Dry run, skip saving node.
      if ($this->config->get('dry_run')) {
        // Print some debug info in case when dry_run flag is on.
        $this->dryRunInfo($print_params);
        return $node;
      }
      // SAVE node finally.
      else {
        if ($node->save()) {
          $status = $this->t('@op Node entity @type "@title" with data from Swapcard.', $print_params);
          $this->notify($node, 'Updated', $status);
          return $node;
        }
      }
    }

    catch (\Exception $e) {
      $print_params['@id'] = $data['id'];
      $print_params['@message'] = $e->getMessage();
      $error = $this->t('Creating @type Node failed on cron run for remote property (Id: @id) with message: @message', $print_params);
      $this->logger->get('swapcard_content')->error($error);
    }
  }

  /**
   * API request, for Events only.
   *
   * @param array $events
   *   An array of Events, ID's set on config.
   * @param string $plugin
   *   An id of the queu plugin being used.
   * @param bool $batch
   *   A flag for to determine if this is batch from UI or Cron run.
   * @param array $batches
   *   Referenced array with batch callbacks.
   *
   * @return array
   *   Associative array with data for Events requested via API.
   */
  public function processEvents(array $events, string $plugin = 'swapcard_event', bool $batch = FALSE, array &$batches = []) {

    $parent_config = $this->configFactory->get('swapcard.settings');
    $guzzle_options = $parent_config->get('guzzle_options');
    $response = [];

    // TEST - add test response array to the queue.
    // Comes from YML file, instad of calling a remote API.
    if ($this->config->get('test') || $this->testPath) {
      $test_file_name = $plugin == 'swapcard_event' ? 'swapcard_events.test.yml' : $plugin . '.test.yml';
      // swapcard_events.test.yml';.
      $test_file_path = static::DUMP_PATH . '/' . $test_file_name;
      if (file_exists($test_file_path)) {
        if (count($events) == 1) {
          $test_event = reset($events);

          $test_events = Yaml::parseFile($test_file_path);
          $event = array_filter($test_events, function ($e) use ($test_event) {
            return $e['id'] == $test_event;
          }, ARRAY_FILTER_USE_BOTH);
          $response['data']['events'] = $event;
        }
        else {
          $response['data']['events'] = Yaml::parseFile($test_file_path);
        }
      }
      else {
        $this->messenger->addError($this->t('Test file <em>@path</em> does not exist. Try checking <em>Dump API response (YML)</em> checkbox and saving and then syncing.', [
          '@path' => $test_file_path,
        ]));
      }
    }
    // DEFAULT request via API.
    else {

      foreach (array_keys($events) as $event_id) {
        $events_ids[] = '\"' . $event_id . '\"';
      }

      $callback_args = [
        'ids' => '[' . implode(',', $events_ids) . ']',
      ] + static::BASE_ARGS;

      $swapcard_plugin_manager = $this->swapcardPluginManager->createInstance($plugin, ['all']);
      $options = [
        'body' => $swapcard_plugin_manager->queryString('events', $callback_args),
      ];

      $response = $swapcard_plugin_manager->post($guzzle_options['base_uri'], $options);
    }

    if (isset($response['data']) && count($response['data']) > 0 && isset($response['data']['events'])) {
      // DUMP TEST file, if that's set on config.
      if ($this->config->get('dump')) {
        $test_file_name = $plugin == 'swapcard_event' ? 'swapcard_events.test.yml' : $plugin . '.test.yml';
        $this->dumpResponse($response['data']['events'], $test_file_name);
      }

      $response_events = [];

      foreach ($response['data']['events'] as $event) {
        $event['node_type'] = 'swapcard_event';
        $event['batch'] = $batch;
        $event['batches'] = $batches;
        $response_events[$event['id']] = $event;
      }

      // Set data for deletion in the state, for the next batch.
      $this->state->set('swapcard_content_events', $response_events);
      return $response_events;
    }
    return $response;
  }

  /**
   * Set Sessions into batch.
   *
   * @param array $sessions_data
   *   Base content array that should contain sessions property as an array.
   * @param bool $batch
   *   A flag for to determine if this is batch from UI or Cron run.
   * @param array $batches
   *   Referenced array with batch callbacks to be executed.
   */
  protected function processSessions(array $sessions_data, bool $batch, array &$batches = []) {

    $entities = $this->checkEventEntities();

    $this->queueFactory->get('swapcard_content_queue_swapcard_event')->createItem($sessions_data);

    // A special handling for Sessions <> Speakers.
    if (isset($entities['speakers_key'])) {
      $sessions_plugin = $this->queueManager->createInstance('swapcard_content_queue_swapcard_session');
      $sessions_plugin->queueSpeakers($sessions_data, $batch);
      if (!in_array('swapcard_content_queue_swapcard_speaker', $batches)) {
        $batches[] = 'swapcard_content_queue_swapcard_speaker';
      }
    }

    if (!in_array('swapcard_content_queue_swapcard_session', $batches)) {
      $batches[] = 'swapcard_content_queue_swapcard_session';
    }
  }

  /**
   * Set Sessions into batch.
   *
   * @param array $exhibitor_data
   *   Base content array that should contain sessions property as an array.
   * @param bool $batch
   *   A flag for to determine if this is batch from UI or Cron run.
   * @param array $batches
   *   Referenced array with batch callbacks to be executed.
   */
  protected function processExhibitors(array $exhibitor_data, $batch, array &$batches) {
    $this->queueFactory->get('swapcard_content_queue_swapcard_event')->createItem($exhibitor_data);
    if (!in_array('swapcard_content_queue_swapcard_exhibitor', $batches)) {
      $batches[] = 'swapcard_content_queue_swapcard_exhibitor';
    }
  }

  /**
   * API request, when some of the entities are selected for syncing.
   *
   * @param array $events
   *   An array of Events, either only ID's from config,
   *   or full data if this is 2nd request.
   * @param string $node_type
   *   Swapcard node type being processed.
   * @param string $swapcard_callback
   *   A unique Swapcard query callback name.
   * @param bool $batch
   *   A flag for to determine if this is batch from UI or Cron run.
   * @param array $batches
   *   Referenced array with batch callbacks to be executed.
   *
   * @return array
   *   Associative array with all the requested data from API.
   */
  protected function processEventEntities(array $events, string $node_type, string $swapcard_callback, bool $batch = FALSE, array &$batches = []) {

    $parent_config = $this->configFactory->get('swapcard.settings');
    $guzzle_options = $parent_config->get('guzzle_options');
    $response = [];

    // TEST - add test response array to the queue.
    // Comes from YML file, instad of calling a remote API.
    if ($this->config->get('test') || $this->testPath) {
      foreach ($events as $event_id => $event) {
        $test_file_path = static::DUMP_PATH . '/' . $node_type . '_' . $event_id . '.test.yml';
        if (file_exists($test_file_path)) {
          $response[$event_id] = Yaml::parseFile($test_file_path);
        }
        else {
          $this->messenger->addError($this->t('Test file <em>@path</em> does not exist. Try checking <em>Dump API response (YML)</em> checkbox and saving and then syncing.', [
            '@path' => $test_file_path,
          ]));
          return [];
        }
      }
    }
    // DEFAULT request via API.
    else {

      foreach ($events as $event_id => $event) {

        $callback_args = [
          'eventId' => '\"' . $event_id . '\"',
        ] + static::BASE_ARGS;

        $swapcard_plugin_manager = $this->swapcardPluginManager->createInstance($node_type, ['all']);
        $options = [
          'body' => $swapcard_plugin_manager->queryString($swapcard_callback, $callback_args),
        ];

        $entities_response = $swapcard_plugin_manager->post($guzzle_options['base_uri'], $options);

        if (is_array($entities_response) && isset($entities_response['data']) && count($entities_response['data']) > 0 && isset($entities_response['data'][$swapcard_callback])) {

          // DUMP TEST file, if that's set on config.
          if ($this->config->get('dump')) {
            $this->dumpResponse($entities_response['data'][$swapcard_callback], $node_type . '_' . $event_id . '.test.yml');
          }

          foreach ($entities_response['data'][$swapcard_callback] as $entity) {
            $response[$event_id][$entity['id']] = $entity;
          }
        }
      }
    }

    // Process raw data now.
    if (!empty($response)) {

      // Add response array to the queue.
      foreach ($response as $event_id => $entities) {

        if (isset($events[$event_id]['id'])) {
          $processed_data[$event_id] = $events[$event_id];
        }
        else {
          $processed_data[$event_id] = [];
        }

        $processed_data[$event_id]['node_type'] = 'swapcard_event';

        if (is_array($entities)) {
          foreach ($entities as $entity) {
            if (is_array($entity) && isset($entity['events']) && isset($entity['events']['nodes']) && !empty($entity['events']['nodes'])) {

              $entity['node_type'] = $node_type;
              $entity['event_id'] = $event_id;
              $entity['batch'] = $batch;
              $entity['batches'] = $batches;

              $processed_data[$event_id][$node_type][$entity['id']] = $entity;

              if (!isset($events[$event_id]['id'])) {
                foreach ($entity['events']['nodes'] as $node) {
                  if ($node['id'] == $event_id) {
                    $processed_data[$event_id] += $node;
                    $processed_data[$event_id] += [
                      'batch' => $batch,
                      'batches' => $batches,
                    ];
                  }
                }
              }
            }
            else {
              $processed_data = [];
            }
          }
        }
        // Property is empty, this event does not have any of
        // exhibitors or sessions right now.
        // else {}.
      }
    }
    return $processed_data;
  }

  /**
   * Fetch existing entities to match with remote response.
   *
   * @param string $entity_type
   *   Entity type id - node, media or paragraph.
   * @param array $existing_entity_properties
   *   Array with entity query properties, field_name|value.
   * @param bool $reset
   *   A flag determining whether to return array of entity objects
   *   or a single entity object.
   *
   * @return array|Drupal\Core\Entity\ContentEntityInterface
   *   Array of entity objects or a single entity object.
   */
  public function existingEntity(string $entity_type, array $existing_entity_properties, bool $reset = TRUE) {

    // An array of entity objects.
    $existing_entities = $this->entityTypeManager->getStorage($entity_type)->loadByProperties($existing_entity_properties);

    if (is_array($existing_entities) && !empty($existing_entities)) {
      if ($reset) {
        $entity = reset($existing_entities);
        return $entity instanceof ContentEntityInterface ? $entity : NULL;
      }
      else {
        return $existing_entities;
      }
    }
    else {
      return $existing_entities instanceof ContentEntityInterface ? $existing_entities : NULL;
    }
    return [];
  }

  /**
   * Dry-run exclusive info.
   *
   * @param array $print_params
   *   The name of the queue to process.
   * @param Drupal\Core\Entity\ContentEntityInterface $entity
   *   Entity being processed.
   */
  public function dryRunInfo(array $print_params, ContentEntityInterface $entity = NULL) {
    // Print some debug info in case when dry_run flag is on.
    if (PHP_SAPI === 'cli') {
      $warning = $this->t('Operating in Dry run mode. @entity entity @type "@title" is marked for updating but not updated.', $print_params);
      print $warning . "\r\n";
      if ($entity && $this->config->get('cron_verbose')) {
        print_r('-------------------------------------------------');
        $this->notify($entity, $print_params['op'], $warning);
      }
    }
  }

  /**
   * A handler to run on Cron exclusively.
   *
   * @param string $queue_name
   *   The name of the queue to process.
   */
  public function claimItems(string $queue_name) {

    try {
      $queue_factory = $this->queueFactory->get($queue_name);
      if ($item = $queue_factory->claimItem()) {
        // Process and delete queue item.
        $this->queueManager->createInstance($queue_name)->processItem($item->data);
        $queue_factory->deleteItem($item);
        return $item;
      }
    }
    catch (\Exception $e) {
      // If any error, send that info to log.
      $this->logger->get('swapcard_content')->error($e->getMessage());
    }
  }

  /**
   * List type fields and custom fields mathing by its ID.
   *
   * @param array $data
   *   Associative array with session or exhibitor values.
   * @param array $swapcard_field
   *   Array with config settings for this field.
   * @param string $drupal_field_name
   *   Machine name of a Drupal field.
   *
   * @return array
   *   An array of swapcard field's values keyed by swapcard field's id.
   */
  protected function processById(array $data, array $swapcard_field, string $drupal_field_name) {
    $fields = [];
    foreach ($data[$swapcard_field['origin']] as $value) {
      if (isset($value['definition']) && isset($value['definition']['id']) && isset($swapcard_field['id'])) {
        if ($value['definition']['id'] == $swapcard_field['id']) {
          if (isset($value['value']) && !empty($value['value'])) {
            $fields[$swapcard_field['id']][] = $value['value'];
          }
          else {
            // Graphql query returns conflict when text and number field
            // are called same time. Swapcard API returns the exact error.
            // The proposed solution is to use "aliases".
            // @see https://www.rakeshjesadiya.com/graphql-fields-conflict-return-conflicting-types-use-different-aliases-on-the-fields/
            // @see https://atheros.ai/blog/how-to-use-graphql-aliases
            foreach (static::FIELD_ALIASES as $alias => $value_name) {
              if (isset($value[$alias])) {
                $fields[$swapcard_field['id']][] = $value[$alias];
              }
            }
          }
        }
      }
    }
    return $fields;
  }

  /**
   * Get our node title from two swapcard fields.
   *
   * These are "firstName" and "lastName".
   *
   * @param array $title_origin
   *   Array value from this field config settings.
   * @param array $data
   *   Cached data or response array retrieved from remote api.
   *
   * @return array
   *   Parsed value ready to implode as a value to our entity title.
   */
  protected function processTitle(array $title_origin, array $data) {
    $title_array = [];
    foreach ($title_origin as $name_field) {
      if ($data[$name_field] && !empty($data[$name_field])) {
        $title_array[] = $data[$name_field];
      }
    }
    return $title_array;
  }

  /**
   * Prepare dates and convert into UTC string to be saved in database.
   *
   *  Note that Swapcard API returns date string in own timezone while,
   *  Drupal saves all in database considering UTC.
   *
   * @param string $drupal_field_name
   *   Machine name of a datetime field.
   * @param string $bundle
   *   Node type where date field is attached.
   * @param string $begins_at
   *   Date string "beginsAt" date property returned from Swapcard API.
   * @param string $ends_at
   *   Date string "endsAt" date property returned from Swapcard API.
   * @param string $timezone
   *   Default timezone string.
   *
   * @return string
   *   Formatted date string with time converted to UTC
   */
  protected function processDate(string $drupal_field_name, string $bundle, string $begins_at, string $ends_at, string $timezone = '') {

    $date_field_storage = $this->entityTypeManager->getStorage('entity_view_display')->load('node.' . $bundle . '.default');

    // First check if there is timezone override on Manage display for a field.
    // Fallback to default timezone settings in Drupal
    // at "/admin/config/regional/settings".
    if (is_object($date_field_storage) && is_object($date_field_storage->getRenderer($drupal_field_name)) && !empty($date_field_storage->getRenderer($drupal_field_name)->getSettings())) {
      $timezone = isset($date_field_storage->getRenderer($drupal_field_name)->getSettings()['timezone_override']) && !empty($date_field_storage->getRenderer($drupal_field_name)->getSettings()['timezone_override']) ? $date_field_storage->getRenderer($drupal_field_name)->getSettings()['timezone_override'] : $timezone;
    }

    // A bit of hardcode like parsing for dates
    // that have space between date and time.
    if (strpos($begins_at, ' ') !== FALSE) {
      $begins_at = str_replace(' ', 'T', $begins_at);
    }

    // A bit of hardcode like parsing for dates
    // that have space between date and time.
    if (strpos($ends_at, ' ') !== FALSE) {
      $ends_at = str_replace(' ', 'T', $ends_at);
    }

    // Create Datetime object with configuration timezone,
    // then return formatted date string with time converted to UTC.
    $start_date = new DrupalDateTime($begins_at, $timezone);
    $end_date = new DrupalDateTime($ends_at, $timezone);

    return [
      'value' => $start_date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT, ['timezone' => 'UTC']),
      'end_value' => $end_date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT, ['timezone' => 'UTC']),
    ];
  }

}
