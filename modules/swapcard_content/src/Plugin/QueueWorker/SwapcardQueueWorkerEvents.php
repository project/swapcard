<?php

namespace Drupal\swapcard_content\Plugin\QueueWorker;

use Drupal\node\NodeInterface;

/**
 * Creates QueueWorker for Swapcard events.
 *
 * @QueueWorker(
 *   id = "swapcard_content_queue_swapcard_event",
 *   title = @Translation("Swapcard Events"),
 *   cron = {"time" = 60}
 * )
 */
class SwapcardQueueWorkerEvents extends SwapcardQueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function saveItem(array $data, array $node_data, NodeInterface $node = NULL) {

    $saved_node = parent::saveItem($data, $node_data);

    $entities = $this->checkEventEntities();
    if (empty($entities)) {
      return $saved_node;
    }

    // We do have some Event's content (entities) as a part of this array.
    $has_exhibitors = isset($entities['entity_key']) && $entities['entity_key'] == 'swapcard_exhibitor';
    $has_sessions = isset($entities['sessions_key']) && $entities['sessions_key'] == 'swapcard_session';

    $is_exhibitors = $has_exhibitors && isset($data['swapcard_exhibitor']) && !empty($data['swapcard_exhibitor']);
    $is_sessions = $has_sessions && isset($data['swapcard_session']) && !empty($data['swapcard_session']);

    if ($is_exhibitors) {
      $this->processEntitites($data, 'swapcard_exhibitor', $saved_node);
      if ($has_sessions) {
        $this->processEntitites($data, 'swapcard_session', $saved_node);
      }
      if ($saved_node instanceof NodeInterface && $this->config->get('purge')) {

        $this->deleteEntities($data, 'swapcard_exhibitor', $saved_node);

        if ($has_sessions) {
          $this->deleteEntities($data, 'swapcard_session', $saved_node);
        }
      }
    }
    else {
      if ($is_sessions) {
        $this->processEntitites($data, 'swapcard_session', $saved_node);

        if ($saved_node instanceof NodeInterface && $this->config->get('purge')) {

          $this->deleteEntities($data, 'swapcard_session', $saved_node);

          if ($has_exhibitors) {
            $this->deleteEntities($data, 'swapcard_exhibitor', $saved_node);
          }
        }

      }
      else {
        if ($has_exhibitors) {
          $this->deleteEntities($data, 'swapcard_exhibitor', $saved_node);
          if ($has_sessions) {
            $this->deleteEntities($data, 'swapcard_session', $saved_node);
          }
        }
        else {
          if ($has_sessions) {
            $this->deleteEntities($data, 'swapcard_session', $saved_node);
          }
        }
      }
    }
    return $saved_node;
  }

  /**
   * Process any entities belonging to this event.
   *
   * Only events get processed on the very first cron so we need this to run
   * the others along the same cycle.
   *
   * @param array $data
   *   The name of the queue to process.
   * @param string $node_type
   *   The type of entity being processed,
   *   either Swapcard exhibitor or Swapcard session.
   * @param \Drupal\node\NodeInterface $saved_node
   *   Parent Swapcard event node object.
   */
  protected function processEntitites(array $data, string $node_type, NodeInterface $saved_node = NULL) {

    if (isset($data[$node_type]) && !empty($data[$node_type])) {

      foreach ($data[$node_type] as &$entity) {
        $entity['event_id'] = $data['id'];
        $entity['field_swapcard_events'] = $saved_node instanceof NodeInterface ? $saved_node->id() : NULL;
      }

      // Add entities to the queue.
      $this->queueFactory->get('swapcard_content_queue_' . $node_type)->createItem($data[$node_type]);
    }
  }

  /**
   * Delete orphan Exhibitors and/or Sessions.
   *
   * @param array $data
   *   Associative array with event's values.
   * @param string $node_type
   *   The type of entity being processed.
   * @param \Drupal\node\NodeInterface $node
   *   Existing Swapcard event node object.
   * @param bool $batch
   *   If true operation will trigger batch process.
   */
  public function deleteEntities(array $data, string $node_type, NodeInterface $node, bool $batch = TRUE) {

    $existing_ids = [];

    $existing_nodes_properties = [
      'type' => $node_type,
    ];

    $existing_nodes_properties['field_swapcard_events.entity:node.nid'] = $node->id();
    $existing_nodes = $this->existingEntity('node', $existing_nodes_properties, FALSE);
    if (!empty($existing_nodes)) {
      $existing_ids = [];
      foreach ($existing_nodes as $existing_node) {
        if ($existing_node->bundle() == $node_type) {
          $unique_field_name = 'field_' . $existing_node->bundle() . '_id';
          if ($existing_node->hasField($unique_field_name) && !empty($existing_node->get($unique_field_name)->getValue())) {
            $existing_id = $existing_node->get($unique_field_name)->getValue()[0]['value'] ?? NULL;
            if ($existing_id) {
              $existing_ids[$existing_id] = $existing_node;
            }
          }
        }
      }
    }

    if (!empty($existing_ids)) {

      $batches = [];

      $compare_data = !isset($data[$node_type]) ? [] : $data[$node_type];
      $diff = array_diff(array_keys($existing_ids), array_keys($compare_data));

      if (!empty($diff)) {
        foreach ($diff as $dd) {
          if ($existing_ids[$dd] instanceof NodeInterface) {
            $batches[$dd] = [
              'entity' => $existing_ids[$dd],
              'data' => $data,
            ];
          }
        }

      }

      // Add this data to deleting queue.
      if (!empty($batches)) {
        $this->queueFactory->get('swapcard_content_queue_swapcard_delete')->createItem($batches);
      }
    }
  }

}
