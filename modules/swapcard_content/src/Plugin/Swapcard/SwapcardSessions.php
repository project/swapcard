<?php

namespace Drupal\swapcard_content\Plugin\Swapcard;

use Drupal\swapcard\Plugin\SwapcardPluginBase;

/**
 * Swapcard sessions query plugin.
 *
 * @Swapcard(
 *   id = "swapcard_session",
 *   admin_label = @Translation("Swapcard Sessions"),
 *   description = @Translation("Swapcard sessions (aka plannings) query plugin"),
 *   fields = {
 *     "title",
 *     "type",
 *     "htmlDescription",
 *     "twitterHashtag",
 *     "place",
 *     "maxSeats",
 *     "updatedAt",
 *     "isPrivate",
 *     "isOverlappingAllowed",
 *     "hideAttendees",
 *     "format",
 *     "endsAt",
 *     "clientIds",
 *     "categories",
 *     "beginsAt",
 *     "bannerUrl",
 *     "accessControlMode",
 *     "fields" = {
 *       "... on NumberField" = {
 *         "id",
 *         "integer: value",
 *         "definition" = {
 *           "id",
 *           "name"
 *         }
 *       },
 *       "... on SelectField" = {
 *         "id",
 *         "value",
 *         "definition" = {
 *           "id",
 *           "name"
 *         },
 *         "translations" = {
 *           "value"
 *         }
 *       },
 *       "... on MultipleSelectField" = {
 *         "id",
 *         "value",
 *         "definition" = {
 *           "id",
 *           "name"
 *         },
 *         "translations" = {
 *           "value"
 *         }
 *       },
 *       "... on TextField" = {
 *         "id",
 *         "value",
 *         "definition" = {
 *           "id",
 *           "name"
 *         }
 *       },
 *       "... on MultipleTextField" = {
 *         "id",
 *         "value",
 *         "definition" = {
 *           "id",
 *           "name"
 *         }
 *       },
 *       "... on LongTextField" = {
 *         "id",
 *         "value",
 *         "definition" = {
 *           "id",
 *           "name"
 *         }
 *       }
 *     },
 *     "events" = {
 *       "nodes" = {
 *         "id",
 *         "title",
 *         "beginsAt",
 *         "endsAt",
 *         "htmlDescription",
 *         "banner" = {
 *           "imageUrl"
 *         }
 *       }
 *     },
 *     "canRegister" = {
 *       "enabled",
 *       "eventGroupsCanRegister" = {
 *         "enabled",
 *         "group" = {
 *           "id",
 *           "name",
 *         }
 *       }
 *     },
 *     "exhibitors" = {
 *       "id",
 *       "name"
 *     },
 *     "speakers" = {
 *       "id",
 *       "firstName",
 *       "lastName",
 *       "organization",
 *       "email",
 *       "biography",
 *       "id",
 *       "jobTitle",
 *       "kind",
 *       "photoUrl",
 *       "websiteUrl",
 *       "socialNetworks" = {
 *         "profile",
 *         "type"
 *       },
 *     }
 *   }
 * )
 */
class SwapcardSessions extends SwapcardPluginBase {}
