<?php

namespace Drupal\swapcard_content\Plugin\Swapcard;

use Drupal\swapcard\Plugin\SwapcardPluginBase;

/**
 * Definition of a Swapcard exhibitors query plugin.
 *
 * @Swapcard(
 *   id = "swapcard_exhibitor",
 *   admin_label = @Translation("Swapcard Exhibitors"),
 *   description = @Translation("Swapcard exhibitors query plugin"),
 *   fields = {
 *    "createdAt",
 *    "updatedAt",
 *    "htmlDescription",
 *    "banner" = {
 *      "imageUrl"
 *    },
 *    "fields" = {
 *      "... on NumberField" = {
 *        "id",
 *        "integer: value",
 *        "definition" = {
 *          "id",
 *          "name"
 *        }
 *      },
 *      "... on SelectField" = {
 *        "id",
 *        "value",
 *        "definition" = {
 *          "id",
 *          "name"
 *        },
 *        "translations" = {
 *          "value"
 *        }
 *      },
 *      "... on MultipleSelectField" = {
 *        "id",
 *        "value",
 *        "definition" = {
 *          "id",
 *          "name"
 *        },
 *        "translations" = {
 *          "value"
 *        }
 *      },
 *      "... on TextField" = {
 *        "id",
 *        "value",
 *        "definition" = {
 *          "id",
 *          "name"
 *        }
 *      },
 *      "... on MultipleTextField" = {
 *        "id",
 *        "value",
 *        "definition" = {
 *          "id",
 *          "name"
 *        }
 *      },
 *      "... on LongTextField" = {
 *        "id",
 *        "value",
 *        "definition" = {
 *          "id",
 *          "name"
 *        }
 *      }
 *    },
 *    "events" = {
 *      "nodes" = {
 *        "id",
 *        "title",
 *        "beginsAt",
 *        "endsAt",
 *        "htmlDescription",
 *        "banner" = {
 *          "imageUrl"
 *        }
 *      }
 *    },
 *    "categories",
 *    "websiteUrl",
 *    "type",
 *    "totalMembers",
 *    "logoUrl",
 *    "name",
 *    "backgroundImageUrl"
 *   }
 * )
 */
class SwapcardExhibitors extends SwapcardPluginBase {}
