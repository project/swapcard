<?php

namespace Drupal\swapcard_content\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Event that is fired when a user logs in.
 */
class SwapcardContent extends Event {

  // This makes it easier for subscribers to reliably use our event name.
  const EVENT_NAME = 'swapcard_content_event';

  /**
   * Original data returned from api.
   *
   * @var array
   */
  public $response;

  /**
   * Processed response data.
   *
   * @var array
   */
  public $data;

  /**
   * Constructs the object.
   *
   * @param array $data
   *   Processed response data.
   * @param array $response
   *   Original data returned from api.
   */
  public function __construct(array $data = [], array $response = []) {
    $this->data = $data;
    $this->response = $response;
  }

}
