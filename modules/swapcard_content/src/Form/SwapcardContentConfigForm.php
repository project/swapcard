<?php

namespace Drupal\swapcard_content\Form;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Queue\QueueWorkerManager;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\swapcard\Form\SwapcardConfigForm;
use Drupal\swapcard\Plugin\SwapcardPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * SwapcardContentConfigForm object definition.
 */
class SwapcardContentConfigForm extends SwapcardConfigForm implements TrustedCallbackInterface {

  /**
   * Drupal\Core\Queue\QueueWorkerManager definition.
   *
   * @var \Drupal\Core\Queue\QueueWorkerManager
   */
  protected $queueManager;

  /**
   * Constructs \Drupal\swapcard_content\Form\SwapcardContentConfigForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager interface.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   Cache interface.
   * @param \Drupal\swapcard\Plugin\SwapcardPluginManager $swapcard_plugin_manager
   *   Swapcard plugin manager.
   * @param \Drupal\Core\Queue\QueueWorkerManager $queue_manager
   *   QueueWorkerManager instance.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, CacheBackendInterface $cache, SwapcardPluginManager $swapcard_plugin_manager, QueueWorkerManager $queue_manager) {
    parent::__construct($config_factory, $entity_type_manager, $cache, $swapcard_plugin_manager);
    $this->queueManager = $queue_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('cache.default'),
      $container->get('plugin.manager.swapcard'),
      $container->get('plugin.manager.queue_worker')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return [
      'processEntities',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildForm($form, $form_state);

    $config = $this->config('swapcard_content.settings');

    $form['swapcard_content'] = [
      '#type' => 'details',
      '#title' => $this->t('Swapcard Content settings'),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];

    // Container for multiple Events.
    $events_id = Html::getId('swapcard-events-wrapper');

    $events_description = $this->t('<ul class="fieldset__description"><li>Use <strong>Add event</strong> and <strong>Remove event</strong> buttons to set IDs for Swapcard events which you want to include in the flow.</li><li>Click on <strong>Sync all</strong> button to synchronise data in Drupal, mirroring what is currently on Swapcard app for Events defined above, as well as selected Entities for each, defined below in the detailed setup. In case of any breaks during the process, errors or similar, you can try running, releasing, or clearing (to start all over) from <a href="/admin/config/system/queue-ui">QueueUI manager</a>.</li></ul>');

    $form['swapcard_content']['events_container'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Events'),
      '#tree' => TRUE,
    ];

    $form['swapcard_content']['events_container'] += $this->multipleEvents($form_state, $events_id, 'events', 'Event');
    $form['swapcard_content']['events_container']['info'] = [
      '#markup' => $events_description,
      '#allowed_tags' => ['ul', 'li', 'a'],
    ];

    $form['swapcard_content']['events_container']['cron'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Sync on cron'),
      '#id' => 'swapcard-content-cron',
      '#default_value' => $config->get('cron'),
      '#description' => $this->t('Check this to run synchronisation ("Sync all") on each cron.'),
    ];

    $form['swapcard_content']['event_content'] = [
      '#type' => 'details',
      '#title' => $this->t('Event content'),
      '#description' => $this->t("Select event's content entities to import for each event."),
      '#open' => TRUE,
    ];

    $form['swapcard_content']['event_content']['entities'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Event entities'),
      '#default_value' => $config->get('entities'),
      '#description' => $this->t("Note that, as per Swapcard design <em>Speakers</em> are child property of sessions."),
      '#id' => 'swapcard-content-entities',
      '#pre_render' => [
        [
          get_class($this), 'processEntities',
        ],
      ],
      '#options' => [
        'swapcard_exhibitor' => $this->t('Exhibitors'),
        'swapcard_session' => $this->t('Sessions'),
        'swapcard_speaker' => $this->t('Speakers'),
      ],
    ];

    $form['swapcard_content']['event_content']['purge'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('"Live" Purge entities'),
      '#default_value' => $config->get('purge'),
      '#description' => $this->t('When checked the obsolete entities, such as Exhibitors, Sessions as well as possibly Speakers, would be deleted. Each of such does not have a relation and/or existence on Swapcard anymore, but previously had, so we would like want to delete it from our image here in Drupal. Think of it as reverse check also may be resources demanding, it has <em>beta</em> status. The same effect, <strong>probably more solid and recommended</strong>, can be achieved with first purging an event, then syncing all over again.'),
      '#states' => [
        'disabled' => [
          [
            ':input[id="swapcard-content-exhibitor-id"]' => ['checked' => FALSE],
            ':input[id="swapcard-content-session-id"]' => ['checked' => FALSE],
          ],
        ],
      ],
    ];

    $node_types = $config->get('node_types');

    $default_fields = [];
    $custom_fields = [];

    foreach ($node_types as $bundle => $fields) {

      foreach ($fields as $drupal_field_name => $swapcard_field) {
        if (isset($swapcard_field['id'])) {
          if (isset($swapcard_field['custom'])) {
            $field_id = 'node.' . $bundle . '.' . $drupal_field_name;
            $custom_fields[] = [
              'drupal_field_name' => $field_id,
              'swapcard_field_id' => $swapcard_field['id'],
            ];
          }
          else {
            $default_fields[$drupal_field_name] = $swapcard_field['id'];
          }
        }
      }
    }

    // Container for multiple items.
    $form['swapcard_content']['event_content']['fields'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Default fields'),
      '#description' => $this->t('Handle fields with pre-defined values in Swapcard app, a dropdown type mostly. With specifying its ID found in Swapcard app here, we are able to pull that pre-defined values and set them as "allowed_values" for these fields/dropdowns in Drupal. That way those fields are always synchronised with remote configuraton.'),
    ];

    $form['swapcard_content']['event_content']['fields']['field_swapcard_session_type'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Session's <em>Type</em> field ID"),
      '#default_value' => $default_fields['field_swapcard_session_type'] ?? NULL,
    ];

    $form['swapcard_content']['event_content']['fields']['field_swapcard_location'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Session's <em>Location</em> field ID"),
      '#default_value' => $default_fields['field_swapcard_location'] ?? NULL,
    ];

    $custom_fields_id = Html::getId('swapcard-fields-wrapper');
    $form['swapcard_content']['event_content']['custom_fields'] = $this->multipleFields($form_state, $custom_fields_id, 'fields', 'Custom field', $custom_fields);

    $default_uid = NULL;

    if ($config->get('default_uid')) {
      $default_uid = $config->get('default_uid');
    }
    else {
      $default_uid = $form_state->getValue(['swapcard_content', 'default_uid']) ? $form_state->getValue([
        'swapcard_content',
        'default_uid',
      ]) : NULL;
    }

    $node_author = $default_uid ? $this->entityTypeManager->getStorage('user')->load($default_uid) : NULL;

    $form['swapcard_content']['default_uid'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Swapcard nodes author'),
      '#description' => $this->t('Select user which becomes author of Swapcard nodes.'),
      '#target_type' => 'user',
      '#default_value' => $node_author,
      '#required' => TRUE,
    ];

    $form['swapcard_content']['advanced'] = [
      '#type' => 'details',
      '#title' => $this->t('Debug'),
      '#description' => $this->t('Some advanced features, mostly for testing and debugging.'),
      '#open' => FALSE,
    ];

    $form['swapcard_content']['advanced']['cron_verbose'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Verbose output'),
      '#default_value' => $config->get('cron_verbose'),
      '#description' => $this->t('Check this to print verbose info in shell, during <em>drush cron</em> for instance. This checbox is disabled if <em>Sync on cron</em> above is not checked.'),
      '#states' => [
        'disabled' => [
          ':input[id="swapcard-content-cron"]' => ['checked' => FALSE],
        ],
        // 'visible' => [
        // ':input[id="swapcard-content-cron"]' => ['checked' => TRUE],
        // ],
      ],
    ];

    // Kill switch, when true does not save anything in Drupal.
    $form['swapcard_content']['advanced']['dry_run'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Dry run'),
      '#default_value' => $config->get('dry_run'),
      '#description' => $this->t('If true no content will be saved in Drupal yet all the other operations will run.'),
    ];

    $form['swapcard_content']['advanced']['dump'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Dump API response (YML)'),
      '#default_value' => $config->get('dump'),
      '#description' => $this->t('When checked "response" data which came from API will be saved into YML file in <em>public://swapcard/tests</em> folder. This is disabled when the next option is checked (loading tests).'),
      '#id' => 'swapcard-content-dump',
      '#states' => [
        'enabled' => [
          ':input[id="swapcard-content-test"]' => ['checked' => FALSE],
        ],
      ],
    ];

    $form['swapcard_content']['advanced']['test'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use dumped response (YML)'),
      '#default_value' => $config->get('test'),
      '#description' => $this->t('When checked "response" data will come locally, from YML files previously dumped in <em>public://swapcard/tests</em>. This is disabled when the previous option is checked (dumping tests). This is the way to avoid too many calls to API but also it may come very benefitial when it is about deleting stuff, such as values for fields and/or even some of the main 3 entities from parent event.'),
      '#id' => 'swapcard-content-test',
      '#states' => [
        'disabled' => [
          ':input[id="swapcard-content-dump"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return $form;
  }

  /**
   * Process entities checkboxes.
   *
   * Basically to disable speakers when no sessions selected.
   *
   * @param array $element
   *   Checkboxes form element.
   */
  public static function processEntities(array $element) {
    $element['swapcard_exhibitor']['#id'] = 'swapcard-content-exhibitor-id';
    $element['swapcard_session']['#id'] = 'swapcard-content-session-id';
    $element['swapcard_speaker']['#states'] = [
      'enabled' => [
        ':input[id="swapcard-content-session-id"]' => ['checked' => TRUE],
      ],
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $config = $this->configFactory->getEditable('swapcard_content.settings');

    // Take care of checkboxes and user.
    $default_uid = $form_state->getValue(['swapcard_content', 'default_uid']);

    $cron = $form_state->getValue([
      'swapcard_content',
      'events_container',
      'cron',
    ]);

    $cron_verbose = $form_state->getValue([
      'swapcard_content',
      'advanced',
      'cron_verbose',
    ]);

    $dump = $form_state->getValue([
      'swapcard_content',
      'advanced',
      'dump',
    ]);

    $test = $form_state->getValue(['swapcard_content', 'advanced', 'test']);
    $dry_run = $form_state->getValue(['swapcard_content', 'advanced', 'dry_run']);
    $purge = $form_state->getValue([
      'swapcard_content',
      'event_content',
      'purge',
    ]);

    $config->set('default_uid', $default_uid);
    $config->set('cron', $cron);
    $config->set('cron_verbose', $cron_verbose);
    $config->set('dump', $dump);
    $config->set('test', $test);
    $config->set('dry_run', $dry_run);
    $config->set('purge', $purge);

    // Take care of event child entities checboxes.
    $event_content = $form_state->getValue([
      'swapcard_content',
      'event_content',
      'entities',
    ]);
    $save_entities = array_filter($event_content, function ($value) {
      return is_string($value) ?? $value;
    });

    $config->set('entities', $save_entities);

    // Take care of event fields entries.
    $default_fields = $form_state->getValue([
      'swapcard_content',
      'event_content',
      'fields',
    ]);

    $custom_fields = $form_state->getValue([
      'swapcard_content',
      'event_content',
      'custom_fields',
      'fields',
    ]);

    $filtered_fields = NULL;
    if (!empty($custom_fields)) {
      $filtered_fields = array_filter($custom_fields, function ($e) {
        return is_numeric($e);
      }, ARRAY_FILTER_USE_KEY);
    }

    $node_types = $config->get('node_types');

    $fields_plugin = $this->queueManager->createInstance('swapcard_content_queue_swapcard_fields');
    $update_fields = $fields_plugin->processFields($default_fields, $filtered_fields, $node_types);

    $config->set('node_types', $update_fields['update_fields']);

    // Take care of events entries.
    $events = $form_state->getValue([
      'swapcard_content',
      'events_container',
      'events',
    ]);
    $save_events = [];

    if (!empty($events)) {
      foreach ($events as $delta => $event) {
        if (is_numeric($delta)) {
          if (isset($event['event_id']) && !empty($event['event_id']) && is_string($event['event_id'])) {
            $event_id = trim($event['event_id']);
            // We do not want the same event more than once.
            if (!in_array($event_id, $save_events)) {
              $save_events[$delta] = $event_id;
            }
          }
          else {
            $save_events[$delta] = NULL;
          }
        }
      }

      // Refresh fields' allowed_values_function() response.
      if ($fields_plugin && !empty($save_events) && isset($update_fields['field_ids']) && !empty($update_fields['field_ids'])) {
        $fields_plugin->queueFactory->get('swapcard_content_queue_swapcard_fields')->createItem($update_fields['field_ids']);
        $fields_plugin->queueUi->batch(['swapcard_content_queue_swapcard_fields']);
      }
    }

    // Save config and return parent method.
    $config->set('events', $save_events);
    $config->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Process bulk synchronisation with data from remote.
   *
   * @param array $form
   *   The form where the settings form is being included in.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function batchSyncSubmit(array &$form, FormStateInterface $form_state) {

    // Submit the form first, save current state in config,
    // to have it available for operations that follow.
    // $form_state->set('sync', TRUE)
    $this->submitForm($form, $form_state);
    // Synchronise data with remote. Run Queue in a batch.
    // Create the SwapcardQueueWorkerEvents plugin instance - mainly because we
    // instance of a QueueWorker that extends SwapcardQueueWorkerBase in order
    // to access services already injected into SwapcardQueueWorkerBase.
    // It _almost_ doesn't matter which QueueWorker we create here, as long as
    // it extends SwapcardQueueWorkerBase.
    $plugin = $this->queueManager->createInstance('swapcard_content_queue_swapcard_event');
    // Create a queue worker item for the initial request to get things
    // started.
    $plugin->queueFactory->get('swapcard_content_queue_swapcard_request')->createItem(['batch' => TRUE]);
    // Add the first two items to the batch that will eventually be run.
    $plugin->queueUi->batch([
      // Syncs option values for known list type (select) fields. This item
      // is created in submitForm().
      'swapcard_content_queue_swapcard_fields',
      // Ultimately, this calls SwapcardQueueWorkerBase::processRequest().
      'swapcard_content_queue_swapcard_request',
    ]);
    // $form_state->set('sync', FALSE);
  }

  /**
   * Purge-obsolete submit.
   *
   * @todo this one.
   *
   * @param array $form
   *   The form where the settings form is being included in.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function purgeEventSubmit(array &$form, FormStateInterface $form_state) {

    $purge_button = $form_state->getTriggeringElement();
    $parents = array_slice($purge_button['#parents'], 0, -1);
    $value = $form_state->getValue($parents);
    if (isset($value['event_id']) && !empty($value['event_id'])) {

      $plugin = $this->queueManager->createInstance('swapcard_content_queue_swapcard_entities');
      $event_node_properties = [
        'field_swapcard_event_id' => $value['event_id'],
      ];
      $event_node = $plugin->existingEntity('node', $event_node_properties);

      // Create a queue worker item for the initial request to get things
      // started.
      if ($event_node instanceof ContentEntityInterface) {
        $data = [
          'id' => $value['event_id'],
          'event_entity' => $event_node,
          'entities' => [],
          'callback' => 'delete',
        ];

        $entities = $plugin->checkEventEntities();
        if (!empty($entities)) {
          if ($entities['entity_key'] == 'swapcard_exhibitor') {
            if (isset($entities['sessions_key'])) {
              $data['entities'][] = 'swapcard_session';
            }
            $data['entities'][] = 'swapcard_exhibitor';
          }
          elseif ($entities['entity_key'] == 'swapcard_session') {
            $data['entities'][] = 'swapcard_session';
          }
        }

        // Add items to the queue.
        $plugin->queueFactory->get('swapcard_content_queue_swapcard_entities')->createItem($data);

        // Add items to the batch.
        $plugin->queueUi->batch(['swapcard_content_queue_swapcard_entities']);
        $form_state->setRedirect('swapcard_content.confirm_purge_form');
      }
      else {
        $warning = $this->t('This event does not exist or it was not synced yet.');
        $plugin->messenger->addWarning($warning);
      }
    }
  }

  /**
   * Sync event.
   *
   * @param array $form
   *   The form where the settings form is being included in.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function syncEventSubmit(array &$form, FormStateInterface $form_state) {

  }

  /**
   * Callback for all ajax actions.
   *
   * @param array $form
   *   The form where the settings form is being included in.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   A parent container element for each group.
   */
  public static function ajaxCallback(array &$form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    $parents = array_slice($trigger['#parents'], 0, -1);
    $element = NestedArray::getValue($form, $parents);
    return $element;
  }

  /**
   * Add multiple element Submit callback.
   *
   * @param array $form
   *   The form where the settings form is being included in.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public static function addItemSubmit(array &$form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    $key = str_replace('op-', '', $trigger['#name']);
    $form_state->set('multiple_keys', $key);
    $num_items = $form_state->get('num_' . $key);
    $delta = $num_items + 1;
    $form_state->set('num_' . $key, $delta);
    $form_state->setRebuild(TRUE);
  }

  /**
   * Remove multiple element Submit callback.
   *
   * @param array $form
   *   The form where the settings form is being included in.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public static function removeItemSubmit(array &$form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    $key = str_replace('op-', '', $trigger['#name']);
    $form_state->set('multiple_keys', $key);
    $num_items = $form_state->get('num_' . $key);
    $delta = $num_items - 1;
    $form_state->set('num_' . $key, $delta);
    $form_state->setRebuild(TRUE);
  }

  /**
   * Calculate current number of multiple fields.
   *
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   * @param string $key
   *   A form element's key.
   * @param array $default_values
   *   An array with current vallues for the field (in config).
   *
   * @return int
   *   A current number of field items within the form.
   */
  private function multipleState(FormStateInterface $form_state, string $key, array $default_values = []) {

    // Gather the number of items in the form already.
    $num_items = $form_state->get('num_' . $key);
    // We have to ensure that there is at least one widget.
    if ($num_items === NULL) {
      if (!empty($default_values) && count($default_values) > 1) {
        $num_items = count($default_values);
      }
      else {
        $num_items = 1;
      }
    }

    $form_state->set('num_' . $key, $num_items);
    return $num_items;
  }

  /**
   * Append add/remove buttons to multiple fields.
   *
   * @param array $element
   *   Referenced form element, "swapcard_content_field" element in this case.
   * @param string $id
   *   Wrapper's unique id.
   * @param string $key
   *   Unique key of a field being manipulated.
   * @param string $label
   *   Label of a field being manipulated.
   * @param int $num_items
   *   Current number of field instances.
   */
  private function multipleOps(array &$element, string $id, string $key, string $label, int $num_items = 1) {

    $element['add_item'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add @label', ['@label' => $label]),
      '#limit_validation_errors' => [],
      '#name' => 'op-' . $key,
      '#submit' => [
        [get_class($this), 'addItemSubmit'],
      ],
      '#weight' => 20,
      '#ajax' => [
        'callback' => [get_class($this), 'ajaxCallback'],
        'wrapper' => $id,
      ],
    ];

    if ($num_items > 1) {

      $element['remove_item'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove @label', ['@label' => $label]),
        '#limit_validation_errors' => [],
        '#name' => 'op-' . $key,
        '#submit' => [
          [get_class($this), 'removeItemSubmit'],
        ],
        '#weight' => 20,
        '#ajax' => [
          'callback' => [get_class($this), 'ajaxCallback'],
          'wrapper' => $id,
        ],
      ];
    }

    if ($key == 'events') {
      // $event_ids = array_filter($element, function ($value) {
      // return isset($value['event_id']) && !empty($value['event_id']) &&
      // isset($value['event_id']['#default_value']);
      // });
      $element['batch_sync'] = [
        '#type' => 'submit',
        '#submit' => ['::batchSyncSubmit'],
        '#value' => $this->t('Sync all'),
        '#weight' => 21,
        // '#disabled' => empty($event_ids),
        '#attributes' => [
          'class' => [
            'button',
            'button--primary',
            'js-form-submit',
            'form-submit',
          ],
        ],
      ];
    }
  }

  /**
   * Process multiple Events form element.
   *
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   * @param string $id
   *   Wrapper's unique id.
   * @param string $key
   *   Unique key of a field being manipulated.
   * @param string $label
   *   Label of a field being manipulated.
   *
   * @return array
   *   Processed form element.
   */
  private function multipleEvents(FormStateInterface $form_state, string $id, string $key, string $label) {

    $element = [];

    // Get current config/values.
    $config = $this->config('swapcard_content.settings');

    // Get curent form state's values.
    $values = $form_state->getValues();

    // Set and get current number of items.
    $num_items = $this->multipleState($form_state, $key, $config->get($key));

    // Container for multiple items.
    $element[$key] = [
      '#type' => 'container',
      '#tree' => TRUE,
      '#attributes' => [
        'id' => $id,
      ],
    ];

    for ($delta = 1; $delta <= $num_items; $delta++) {

      $index = $delta - 1;
      $item = NULL;

      if (is_array($config->get($key)) && !empty($config->get($key))) {
        $item = isset($config->get($key)[$index]) && !empty($config->get($key)[$index]) ? $config->get($key)[$index] : NULL;
      }
      else {
        if (isset($values[$key]) && !empty($values[$key])) {
          $item = isset($values[$key][$index]) && !empty($values[$key][$index]) ? $values[$key][$index] : NULL;
        }
      }

      // Child item.
      $item_id = Html::getId('swapcard-' . $key . '-' . $index);
      $element[$key][$index] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => [
            'container-inline',
          ],
        ],
      ];
      $element[$key][$index]['event_id'] = [
        '#type' => 'textfield',
        '#title' => $this->t('@label ID', ['@label' => $label]),
        '#default_value' => $item,
        '#attributes' => [
          'id' => $item_id,
        ],
      ];

      $element[$key][$index]['sync_event'] = [
        '#type' => 'submit',
        '#submit' => ['::syncEventSubmit'],
        '#value' => $this->t('Sync'),
        '#name' => str_replace('-', '_', $item_id . '-sync'),
        '#disabled' => TRUE,
      ];

      $element[$key][$index]['purge_event'] = [
        '#type' => 'submit',
        '#submit' => ['::purgeEventSubmit'],
        '#value' => $this->t('Purge'),
        '#name' => str_replace('-', '_', $item_id . '-purge'),
      ];
    }

    // Render common add/remove ajax buttons.
    $this->multipleOps($element[$key], $id, $key, $label, $num_items);
    return $element;
  }

  /**
   * Process multiple Custom fields form element.
   *
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   * @param string $id
   *   Wrapper's unique id.
   * @param string $key
   *   Unique key of a field being manipulated.
   * @param string $label
   *   Label of a field being manipulated.
   * @param array $fields
   *   Array of existing custom fields.
   *
   * @return array
   *   Processed form element.
   */
  private function multipleFields(FormStateInterface $form_state, string $id, string $key, string $label, array $fields = []) {

    $element = [];

    // Set and get current number of items.
    $num_items = $this->multipleState($form_state, $key, $fields);

    // Container for multiple items.
    $element[$key] = [
      '#type' => 'container',
      '#tree' => TRUE,
      '#attributes' => [
        'id' => $id,
      ],
    ];

    $item = [
      'drupal_field_name' => NULL,
      'swapcard_field_id' => NULL,
    ];

    for ($delta = 1; $delta <= $num_items; $delta++) {

      $index = $delta - 1;
      $item = [
        'drupal_field_name' => NULL,
        'swapcard_field_id' => NULL,
      ];

      if (!empty($fields) && isset($fields[$index]) && !empty($fields[$index])) {
        $item = $fields[$index];
      }

      if (isset($item['drupal_field_name'])) {
        $item['drupal_field_name'] = is_string($item['drupal_field_name']) ? $this->entityTypeManager->getStorage('field_config')->load($item['drupal_field_name']) : $item['drupal_field_name'];
      }

      // Child item.
      $element[$key][$index] = [
        '#type' => 'swapcard_content_custom_field',
        '#title' => $this->t('Custom field'),
        '#description' => $this->t('Handle custom created fields in Swapcard app.'),
        '#attributes' => [
          'id' => $id,
        ],
        '#default_value' => $item,
      ];

    }

    // Render common add/remove ajax buttons.
    $this->multipleOps($element[$key], $id, $key, $label, $num_items);
    return $element;

  }

}
