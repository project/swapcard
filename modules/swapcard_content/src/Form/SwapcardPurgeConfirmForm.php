<?php

namespace Drupal\swapcard_content\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueWorkerManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ConfirmClearForm declaration.
 *
 * @package Drupal\swapcard_content\Form
 */
class SwapcardPurgeConfirmForm extends ConfirmFormBase {

  /**
   * The State interface.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Renderer interface.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Queue worker manager interface.
   *
   * @var \Drupal\Core\Queue\QueueWorkerManagerInterface
   */
  protected $queueWorkerManager;

  /**
   * Queue factory instance.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * ConfirmClearForm constructor.
   *
   * @param \Drupal\Core\TempStore\StateInterface $state
   *   The tempstore factory.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory interface.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The messenger service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   Renderer service.
   * @param \Drupal\Core\Queue\QueueWorkerManagerInterface $queueWorkerManager
   *   Queue worker manager service.
   * @param \Drupal\Core\Queue\QueueFactory $queueFactory
   *   Queue factory instance.
   */
  public function __construct(StateInterface $state, ConfigFactoryInterface $config_factory, Messenger $messenger, RendererInterface $renderer, QueueWorkerManagerInterface $queueWorkerManager, QueueFactory $queueFactory) {
    $this->state = $state;
    $this->configFactory = $config_factory;
    $this->messenger = $messenger;
    $this->renderer = $renderer;
    $this->queueWorkerManager = $queueWorkerManager;
    $this->queueFactory = $queueFactory;
  }

  /**
   * {@inheritdoc}
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The current service container.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('state'),
      $container->get('config.factory'),
      $container->get('messenger'),
      $container->get('renderer'),
      $container->get('plugin.manager.queue_worker'),
      $container->get('queue')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'swapcard_content_purge_confirm_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    $count = '';
    $title = '';
    $batches_data = $this->getBatchesData();
    if (is_array($batches_data)) {

      $count = count($batches_data);
      $first = reset($batches_data);
      if (isset($first['data']) && isset($first['data']['event_entity']) && $first['data']['event_entity'] instanceof ContentEntityInterface) {
        $title = $first['data']['event_entity']->label();
      }
    }

    return $this->t('Are you sure you want to purge event %title and @count of its entities?', [
      '%title' => $title,
      '@count' => $count,
    ]);

  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {

    if ($state_data = $this->state->get('swapcard_content_entities')) {
      if (isset($state_data['batches_data']) && !empty($state_data['batches_data'])) {

        $items = $this->parseBatchesData($state_data['batches_data']);

        $title = $this->t('The list of entities to be deleted. <p class="form-item__label">Note: if there are <em>Swapcard sessions</em> to be deleted batch will look for possible orphan <em>Swapcard speakers</em> and delete such, those are not listed below nor calculated into a number of items stated above.</p>');

        $text = [
          '#type' => 'container',
          'list' => [
            '#theme' => 'item_list',
            '#title' => $title,
            '#type' => 'ul',
            '#items' => $items,
          ],
          // 'description' => [
          // '#plain_text' => $this->t(''),
          // ],
        ];
        return $this->renderer->render($text);
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('swapcard.config');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    if ($state_data = $this->state->get('swapcard_content_entities')) {
      if (isset($state_data['batches_data']) && !empty($state_data['batches_data'])) {

        $batch_data = $state_data['batches_data'];

        $first = reset($state_data['batches_data']);
        if (isset($first['data']) && isset($first['data']['event_entity']) && $first['data']['event_entity'] instanceof ContentEntityInterface) {

          $event_id = $first['data']['id'];
          $event_entity = $first['data']['event_entity'];
          $batch_data[$event_id] = [
            'entity' => $event_entity,
            'data' => [
              'id' => $event_id,
              'event_entity' => $event_entity,
              'callback' => 'delete',
            ],
            // 'id' => $event_id,
          ];
        }

        $plugin = $this->queueWorkerManager->createInstance('swapcard_content_queue_swapcard_delete');

        foreach ($batch_data as $batch) {
          $batch['data']['batch'] = TRUE;
          $plugin->queueFactory->get('swapcard_content_queue_swapcard_delete')->createItem($batch);
        }

        $batches = ['swapcard_content_queue_swapcard_delete'];

        // Add the other modules ability to extend batches before final run.
        $plugin->moduleHandler->alter('swapcard_purge_batches', $batches, $batch_data);

        // Add delete batch.
        $plugin->queueUi->batch($batches);

        if ($config = $this->configFactory->getEditable('swapcard_content.settings')) {
          $filtered_events = array_filter($config->get('events'), function ($value) use ($event_id) {
            return $value != $event_id;
          });

          $config->set('events', array_values($filtered_events));
          $config->save();
        }
      }

      $this->state->delete('swapcard_content_entities');
    }

    $form_state->setRedirect('swapcard.config');
  }

  /**
   * Retrieve data array for batches, from the current state.
   */
  private function getBatchesData() {
    if ($state_data = $this->state->get('swapcard_content_entities')) {
      if (isset($state_data['batches_data']) && !empty($state_data['batches_data'])) {
        return $state_data['batches_data'];
      }
    }
  }

  /**
   * Parse batches data array for title and description.
   */
  private function parseBatchesData(array $batches_data) {
    $items = [];
    $diff = count($batches_data) - 150;
    $index = 0;
    foreach ($batches_data as $batch) {
      if (isset($batch['entity']) && $batch['entity'] instanceof ContentEntityInterface) {
        if ($index <= 149) {
          $items[$batch['entity']->id()] = $this->t('@title [%type]', [
            '@title' => $batch['entity']->label(),
            '%type' => ucfirst(str_replace('_', ' ', $batch['entity']->bundle())),
          ]);
          $index++;
        }

      }
    }
    if (!empty($items) && $diff > $index) {
      $items['x_more'] = $this->t('... <strong>and @diff more</strong>.', ['@diff' => $diff]);
    }
    return $items;
  }

}
