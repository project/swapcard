<?php

/**
 * @file
 * Hooks provided by the Swapcard content module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter current API request, either fields or args or similar.
 *
 * @param array $batches
 *   An array with existing batches ids.
 * @param array $data
 *   Associative array with values retrieved from Swapcard API.
 */
function hook_swapcard_batches_alter(array &$batches, array $data = []) {
  // Do some extra processing, maybe.
  $response_data = my_function_process($data);
  // Then add to existing batch, "Sync" action.
  if (!empty($response_data)) {
    $batches[] = '[existing_or_custom_queue_worker_plugin_name]';
  }
}

/**
 * Alter response array right after it was retrieved and structured.
 *
 * @param array $data
 *   Associative array with values sent to request.
 * @param array $response
 *   Associative array with values retrieved from Swapcard API.
 */
function hook_swapcard_content_response_alter(array $data, array &$response = []) {
  // Add some custom data to response array.
  if (!empty($response)) {
    $add_data = my_shared_fields($response);
    if (is_array($add_data) && !empty($add_data)) {
      $response += $add_data;
    }
  }
}

/**
 * Alter an array with entity's fields/values just before it gets saved.
 *
 * @param array $data
 *   Associative array with values retrieved from Swapcard API.
 * @param array $node_data
 *   An array with entity's fields/values.
 * @param array $fields
 *   Associative array with fields mapped in this module's config settings.
 * @param Drupal\node\NodeInterface $node
 *   A parent node, usually Event node. Can be a Session node, for Speakers.
 */
function hook_swapcard_content_prepare_alter(array &$data, array &$node_data, array &$fields, NodeInterface $node = NULL) {
  // Add custom property/data to node's data ready for saving.
  $shared_fields = \Drupal::service('config.factory')->get('[my_module.settings]')->get('shared_fields');
  if (is_array($shared_fields) && !empty($shared_fields)) {
    foreach ($shared_fields as $drupal_field_name => $shared_field) {
      foreach ($shared_field as $shared_field_value) {
        if (is_string($shared_field_value) && in_array($shared_field_value, array_keys($data))) {
          $node_data[$drupal_field_name] = $data[$shared_field_value];
        }
        else {
          $node_data[$drupal_field_name] = NULL;
        }
      }
    }
  }
}

/**
 * Dome something when a batch of entities is just saved.
 *
 * @param array $data
 *   Associative array with values sent to request.
 * @param array $nodes
 *   An array with node entities that were saved in current batch.
 */
function hook_swapcard_content_post_save_alter(array $data, array $nodes) {
  foreach ($nodes as $node) {
    if ($node->bundle() == 'my_content_type') {
      // Do something with this node entity.
    }
  }
}

/**
 * Alter response array right after it was retrieved and structured.
 *
 * @param array $data
 *   Associative array with values sent to request.
 * @param array $response
 *   Associative array with values retrieved from Swapcard API.
 */
function hook_swapcard_content_purge_alter(array $data, array &$response = []) {
}
