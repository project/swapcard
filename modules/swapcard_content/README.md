### Swapcard Content

Module implementing Swapcard API, creating and synchronising
diverse content/entities from Swapcard into Drupal.

NOTE: Enabling it will create 4 content types:
- Swapcard Events
- Swapcard Sessions
- Swapcard Speakers
- Swapcard Exhibitors
With all of the belonging fields. For more info on that check out
`swapcard_content.settings.yml.`

This module extends base module's Config form adding quite some new options, so
if you enable it you should visit _/admin/config/services/swapcard/config_ and
set some preference there. From defining fields with pre-defined values
(i.e. select lists on Swapcard) to using test responses etc.

Running "Sync All" action from Config form will map and synchronise data
in entities in Drupal with those on Swapcard. Same is for doing it on cron,
if that option is selected on config form, or via module's drush commands.
This action actually works on media entities attached to nodes
(if _swapcard_content_media_ module is enabled), for images fields on Swapcard,
as well as for relations between entities such as entity_reference field from
all of the children to a parent Event as well as some specific such as
Sessions <> Exhibitors.

#### Hooks
@see [swapcard_content.api.php](./swapcard_content.api.php) for hooks available.

#### Drush commands

```
// Sync all the events set on config form.
drush swc-sync

// Sync particular event.
// Note, this will add the event id in config if not already there.
drush swc-sync [event_id]

// Purge all the events set on config form.
drush swc-purge

// Purge particular event.
// Note, this will remove the event id from config.
drush swc-purge [event_id]

// Purge particular event with an additional batch to run.
drush swc-purge [event_id] --batches='entity_reference_revisions_orphan_purger'
```
