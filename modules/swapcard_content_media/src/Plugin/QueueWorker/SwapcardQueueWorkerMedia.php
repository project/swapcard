<?php

namespace Drupal\swapcard_content_media\Plugin\QueueWorker;

use Drupal\Component\Utility\Crypt;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\media\MediaInterface;
use Drupal\swapcard_content\Plugin\QueueWorker\SwapcardQueueWorkerBase;

/**
 * Creates QueueWorker for Swapcard media.
 *
 * @QueueWorker(
 *   id = "swapcard_content_queue_swapcard_media",
 *   title = @Translation("Swapcard Media"),
 *   cron = {"time" = 90}
 * )
 */
class SwapcardQueueWorkerMedia extends SwapcardQueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {

    if (empty($data) || !isset($data['entity'])) {
      return;
    }

    if ($data['entity'] instanceof MediaInterface) {
      if ($data['entity']->save()) {
        $op = $data['op'] ?? 'Updated';
        $this->notify($data['entity'], $op);
      }
    }
  }

  /**
   * Request and save remote file in Drupal.
   *
   * @param string $id
   *   Remote entity unique key.
   * @param string $node_type
   *   Content type we are reating/updating in this round.
   * @param string|null $image_source
   *   Absolute URL to remote image, as returned from response.
   *
   * @return mixed
   *   Saved File entity or false.
   */
  protected function processFile(string $id, string $node_type, string $image_source = NULL) {

    if (!$image_source || empty($image_source)) {
      return;
    }

    try {
      $destination = 'public://swapcard/' . $node_type . '/' . $id;
      $this->fileSystem->prepareDirectory($destination, FileSystemInterface::CREATE_DIRECTORY);

      if ($file = system_retrieve_file($image_source, $destination, TRUE, FileSystemInterface::EXISTS_RENAME)) {
        return $file;
      }
    }
    catch (\Exception $e) {
      $error = $this->t('Creating File from remote Swapcard image failed on cron run (File url: @file_url) with message: @message', [
        '@file_url' => $image_source,
        '@message' => $e->getMessage(),
      ]);
      $this->logger->get('swapcard_content')->error($error);
    }
  }

  /**
   * Determine images source.
   *
   * By Swapcard design response properties are fairly varying per entity.
   *
   * @param array $data
   *   Associative array with values retrieved from Swapcard, for this entity.
   * @param array $node_data
   *   Array with reference on already populated node data.
   * @param Drupal\node\NodeInterface $node
   *   Existing node in Drupal for this Swapcard entity.
   */
  public function processMediaSource(array $data, array &$node_data, ContentEntityInterface $node = NULL) {

    $image_source = NULL;
    $config = $this->configFactory->get('swapcard_content_media.settings');

    if (!$node) {
      $node_properties['field_' . $node_data['type'] . '_id'] = $data['id'];
      $node = $this->existingEntity('node', $node_properties) ?? $this->existingEntity('node', $node_properties);
    }

    $origin = $config->get('node_types')[$node_data['type']]['origin'];

    // The following is far from ideal,
    // for now we support on two-level nesting for properties.
    // This is the case for Event with "origin: { banner: imageUrl }".
    if (is_array($origin)) {

      $parent_property_array = array_keys($origin);
      $parent_property = reset($parent_property_array);
      if (is_string($parent_property) && isset($data[$parent_property])) {
        $child_property = NestedArray::getValue($origin, [$parent_property]);

        if (is_string($child_property) && isset($data[$parent_property][$child_property])) {
          $image_source = $data[$parent_property][$child_property];
        }
      }
    }
    // Yet all the other types have single level such as "origin: bannerUrl".
    elseif (is_string($origin) && isset($data[$origin])) {
      $image_source = $data[$origin];
    }

    // Events and Sessions have "title" property.
    if (isset($data['title']) && !empty($data['title'])) {
      $media_name = $data['title'];
    }
    // Exhibitors have "name" property.
    elseif (isset($data['name']) && !empty($data['name'])) {
      $media_name = $data['name'];
    }
    // Try alternative (appears such in Speakers for now).
    elseif (isset($data['firstName']) && isset($data['lastName'])) {
      $media_name = $data['firstName'] . ' ' . $data['lastName'];
    }
    // Media's "name" property should NOT remain empty.
    else {
      $media_name = static::PLACEHOLDER_TITLE;
    }

    $drupal_field_name = $config->get('field_name');

    if ($image_source) {
      if ($media = $this->processMedia($data, $node_data, $image_source, $media_name, $node)) {
        $node_data[$drupal_field_name] = [
          'target_id' => $media->id(),
        ];
      }
      else {
        $node_data[$drupal_field_name] = NULL;
      }
    }
    else {
      $node_data[$drupal_field_name] = NULL;
    }
  }

  /**
   * Prepare Media entity attached to node.
   *
   * @param array $data
   *   Associative array with values retrieved from Swapcard, for this entity.
   * @param array $node_data
   *   Array with reference on already populated node data.
   * @param string|null $image_source
   *   Absolute URL to remote image, as returned from response.
   * @param string|null $media_name
   *   Title for Media entity, rendered from remote data.
   * @param object|null $existing_node
   *   Existing node object for given entity or null.
   *
   * @return mixed
   *   Saved media entity or false.
   */
  public function processMedia(array $data, array $node_data, string $image_source = NULL, string $media_name = NULL, ContentEntityInterface $existing_node = NULL) {

    $config = $this->configFactory->get('swapcard_content_media.settings');
    $media_storage = $this->entityTypeManager->getStorage('media');

    $drupal_field_name = $config->get('field_name');

    // There is no image on remote, it was deleted after this node
    // was created with originally existing that remote image.
    if ($existing_node && (is_null($image_source) || empty($image_source))) {

      $existing_media_id = $existing_node->hasField($drupal_field_name) && !empty($existing_node->get($drupal_field_name)->getValue()) && isset($existing_node->get($drupal_field_name)->getValue()[0]['target_id']) ? $existing_node->get($drupal_field_name)->getValue()[0]['target_id'] : NULL;

      if ($existing_media_id) {

        $media = $media_storage->load($existing_media_id);

        if ($media instanceof MediaInterface) {

          $file_field = $config->get('media_field_name');

          $file_id = $media->hasField($file_field) && !empty($media->get($file_field)->getValue()) ? $media->get($file_field)->getValue()[0]['target_id'] : NULL;

          // Set now belonging File entity to temporary.
          if ($file_id) {
            if ($file = $this->entityTypeManager->getStorage('file')->load($file_id)) {
              $file->setTemporary();
              $file->save();
              // $file->delete();
            }
          }

          if (!$media_name) {
            $media_name = $media->getName();
          }

          $other_parents_properties = [];
          $other_parents_properties['field_swapcard_image.entity:media.mid'] = $media->id();

          $other_parents = $this->existingEntity('node', $other_parents_properties, FALSE);

          // Queue media entity for deleting.
          if (empty($other_parents) || is_null($other_parents)) {

            $queue_data[$media->id()] = [
              'entity' => $media,
              'data' => $data,
            ];

            return $queue_data;
          }
          return FALSE;
        }
      }
    }
    else {
      // Query existing by this fields.
      $existing_properties = [
        'bundle' => $config->get('media_bundle'),
        'field_swapcard_id' => $image_source ? Crypt::hashBase64($image_source) : Crypt::hashBase64($media_name),
      ];

      // Check for exiting media entity.
      $existing_media = $this->existingEntity('media', $existing_properties);
      // There were no any changes on this media,
      // so return with no actions.
      if ($existing_media instanceof MediaInterface) {
        return $existing_media;
      }
    }

    try {

      if ($file = $this->processFile($data['id'], $node_data['type'], $image_source)) {

        // Base, mandatory media data.
        $media_data = [
          'uid' => $this->author,
          'bundle' => $config->get('media_bundle'),
          'field_swapcard_id' => $image_source ? Crypt::hashBase64($image_source) : Crypt::hashBase64($media_name),
        ];

        $media_data[$config->get('media_field_name')] = [
          'target_id' => $file->id(),
          'alt' => Html::getId($media_name . '_' . $data['id']),
          'title' => $media_name,
        ];

        $media = NULL;

        if ($existing_node) {
          $op = 'Updated';
          $existing_media_id = $existing_node->hasField($drupal_field_name) && !empty($existing_node->get($drupal_field_name)->getValue()) ? $existing_node->get($drupal_field_name)->getValue()[0]['target_id'] : NULL;

          // This node had other image previously, let's update.
          if ($existing_media_id) {
            $media = $media_storage->load($existing_media_id);
            if ($media instanceof MediaInterface) {
              foreach ($media_data as $field_name => $field_value) {
                $media->set($field_name, $field_value);
              }
            }
          }
          // This node did not have image previously, let's create it.
          else {
            $op = 'Created';
            $media = $media_storage->create($media_data);
          }
        }
        // New node therefore new media entity as well.
        else {
          $op = 'Created';
          $media = $media_storage->create($media_data);
        }

        if ($media instanceof MediaInterface) {

          $media->setName($media_name)->setPublished(TRUE);

          if ($media->save()) {
            $this->notify($media, $op);
          }

          return $media;
        }
      }
    }
    catch (\Exception $e) {
      $error = $this->t('Creating Media entity from Swapcard image failed on cron run (File Id: @id) with message: @message', [
        '@id' => $file->id(),
        '@message' => $e->getMessage(),
      ]);
      $this->logger->get('swapcard_content')->error($error);
    }
  }

}
