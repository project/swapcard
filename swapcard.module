<?php

/**
 * @file
 * swapcard.module
 */

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function swapcard_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {

    case 'help.page.swapcard':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= t('Enables API connections with Swapcard, with a plugin manager for implementing its various callbacks. <a target="_blank" href="@url">Swapcard</a> is a modern web platform for managing all kind of events and related activities.', ['@url' => 'https://swapcard.com']);
      $output .= '<h3>' . t('Purpose') . '</h3>';
      $output .= t('This module is designed for Drupal developers and only provides a shell for invoking Swapcard API requests. Before any usage please make sure to check out <a href="@api_url" target="_blank">Swapcard API</a> as well as their <a href="@explorer_url" target="_blank">Graphql explorer</a>.', [
        '@api_url' => 'https://developer.swapcard.com',
        '@explorer_url' => 'https://developer.swapcard.com/content-api/explorer',
      ]);

      $output .= '<h3>' . t('Uses') . '</h3>';
      $output .= '<dt>' . t('Go to Swapcard administration page and paste your API key there. That will automatically trigger a request to validate that connection with remote api works.', ['@config_url' => '/admin/config/services/swapcard/config']) . '</dt>';
      $output .= '<dt>' . t("See module's <strong>README.md</strong> file for examples of usage as a plugin, in the code.");
      return $output;

    case 'swapcard.config':
      return '<dt>' . t('<strong>Swapcard</strong> module is designed for Drupal developers and only provides a shell for invoking Swapcard API requests. Before any usage please make sure to check out <a href="@api_url" target="_blank">Swapcard API</a> as well as their <a href="@explorer_url" target="_blank">Graphql explorer</a>.', [
        '@api_url' => 'https://developer.swapcard.com',
        '@explorer_url' => 'https://developer.swapcard.com/content-api/explorer',
      ]) . '</dt>';
  }
}
