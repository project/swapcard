## Swapcard   

### Overview

Enables API connections with [Swapcard](https://swapcard.com),
with a plugin manager for implementing its various callbacks.
Swapcard is a modern web platform for managing
all kind of events and related activities.
This module contains base module (API implementation) **Swapcard** as well as
sort of example module **Swapcard Content** that creates and keeps in sync
several possible (to opt) content types in Drupal with source in Swapcard app.

Create it with plugin's default fields for Graphql query
or with your own choice of query fields.

### Swapcard module

It's a main module and defines Swapcard plugin, which takes care of formatting
Swapcard's Graphql queries and sends request to its API using [Guzzle](https://docs.guzzlephp.org/en/stable/).
It defines a base Config form at _/admin/config/services/swapcard/config_
that  is designed for Drupal developers and only provides
a shell for invoking Swapcard API requests.
Before any usage please make sure to check out [Swapcard API](https://developer.swapcard.com/) page
as well as their [Graphql explorer](https://developer.swapcard.com/api/graphql-event-api/graphql-explorer/explorer/).

Visit "Configuration > Web services > Swapcard"
and paste your API key. That will automatically trigger a request
to validate that connection with remote api works. Try something like this:

```php
/**
 * Get default GuzzleHttp options from module's config.
 */
$guzzle_options = \Drupal::config('swapcard.settings')->get('guzzle_options');

$fields = ['all'];

/* Now create provided "events" plugin instance. */
$events_plugin = \Drupal::service('plugin.manager.swapcard')
->createInstance('swapcard_events', $fields);

/**
 * Use plugin's queryString method and
 * extend api request params with a value returned.
 * @param string 'events'
 *  The name of Graphql query callback.
 */
$graphql = $events_plugin->queryString('events');

/* Extend GuzzleHttp request params with body payload param. */
$options = ['body' => $graphql];

/* Run the request. */
$response = $events_plugin->post($guzzle_options['base_uri'], $options);    

/* The following should print array of retrieved events. */
var_dump($response);
```

### Swapcard Content module

Requires [Queue UI](https://www.drupal.org/project/queue_ui) module.
Module implementing Swapcard API, creating and synchronising
diverse content/entities from Swapcard into Drupal.

##### @see more about it, hooks and drush commands, in its own [README](./modules/swapcard_content/README.md) file.

NOTE: Enabling it will create 4 content types:
- Swapcard Events
- Swapcard Sessions
- Swapcard Speakers
- Swapcard Exhibitors
With all of the belonging fields. For more info on that check out
`swapcard_content.settings.yml.`

This module extends base module's Config form adding quite some new options, so
if you enable it you should visit _/admin/config/services/swapcard/config_ and
set some preference there. From defining fields with pre-defined values
(i.e. select lists on Swapcard) to using test responses etc.

Running _Sync_ action from Config form will map and synchronise data in entities
in Drupal with those on Swapcard. Same is for having it doing on cron if that
option is selected on config form. The whole scenario includes relational logic
between entities in form of entity_reference field in Drupal. Such as from all
entities to parent Event as well as some specific such as
Sessions <> Exhibitors
and Sessions > Speakers.

This module implements _QueueWorker_ plugin and is sort of designed
(tested) on big response data arrays - around 2500 nodes in batch, of which
almost each contains Media entity attached and includes mentioned possible
relations between entities.

Swapcard app has possibility for creating custom fields and that is also
supported by this module. Additionally, _List_ type of fields
(i.e. dropdown or checkboxes) are designed to mirror and keep in sync with
the equivalent field definitions on Swapcard app so _#options_ (key|value) are
always the most accurate. This is done though Drupal's field storage method
_allowed_values_function_.

### Swapcard Content Media module

Creates Media reference field on Swapcard content types and implements syncing
various Swapcard images:
- Event banner
- Exhibitor logo
- Session banner
- Speaker photo

### Known problems

1. By Swapcard's design _Speakers_ are shared between Events, yet not having
unique "global" id at the same time, but unique one per event.
Because the idea here is to try to NOT repeat same speaker (as a node) in Drupal
we match those by email and name. Very unreliable overall.
2. Even though each entity has _updatedAt_ property as a part of
response object, that value is only valid for the entity itself and does not
reflect if related (or child) entity(ies) attached to this one were updated,
some of their values changed. This is why syncing will run on matching all,
and every time either on cron or when manually running a batch from config form.
As per current information Swapcard developers are working on this one!  

#### Authors/Credits

* [nk_](https://www.drupal.org/u/nk_)
* [ultimike](https://www.drupal.org/u/ultimike)
