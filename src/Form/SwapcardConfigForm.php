<?php

namespace Drupal\swapcard\Form;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\swapcard\Plugin\SwapcardPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * SwapcardConfigForm object definition.
 */
class SwapcardConfigForm extends ConfigFormBase {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Cache\CacheBackendInterface definition.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * Drupal\swapcard\Plugin\SwapcardPluginManager definition.
   *
   * @var \Drupal\swapcard\Plugin\SwapcardPluginManager
   */
  protected $swapcardPluginManager;

  /**
   * Constructs a \Drupal\swapcard\Form\SwapcardConfigForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager interface.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   Cache interface.
   * @param \Drupal\swapcard\Plugin\SwapcardPluginManager $swapcard_plugin_manager
   *   Swapcard plugin manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, CacheBackendInterface $cache, SwapcardPluginManager $swapcard_plugin_manager) {

    parent::__construct($config_factory);

    $this->entityTypeManager = $entity_type_manager;
    $this->cache = $cache;
    $this->swapcardPluginManager = $swapcard_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('cache.default'),
      $container->get('plugin.manager.swapcard')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'swapcard_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'swapcard.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildForm($form, $form_state);

    $config = $this->config('swapcard.settings');

    // Upon pasting Api key this will ping remote host to validate.
    $this->pingApi($form_state, $config);

    $form['swapcard_api'] = [
      '#type' => 'details',
      '#title' => $this->t('Swapcard API settings'),
      '#open' => FALSE,
      '#tree' => TRUE,
      '#prefix' => '<div id="swapcard-api-wrapper">',
      '#suffix' => '</div>',
    ];

    // Credentials.
    $form['swapcard_api']['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#default_value' => $config->get('api_key'),
      '#required' => TRUE,
      '#description' => $this->t('Paste your API key here, script will automatically ping remote api to test connection.'),
      '#ajax' => [
        'event' => 'keyup',
        'callback' => [get_class($this), 'getEvents'],
        'effect' => 'fade',
        'wrapper' => 'swapcard-api-wrapper',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Requesting Events from Swapcard...'),
        ],
      ],
    ];

    if ($config->get('api_key')) {
      $form['swapcard_api']['api_key']['#ajax']['event'] = 'change';
    }

    $form['swapcard_api']['base_uri'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Base URI'),
      '#default_value' => $config->get('guzzle_options')['base_uri'],
      '#required' => TRUE,
    ];

    $form['swapcard_api']['timeout'] = [
      '#type' => 'number',
      '#title' => $this->t('Timeout'),
      '#default_value' => $config->get('guzzle_options')['timeout'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $config = $this->config('swapcard.settings');

    $api_key = $form_state->getValue([
      'swapcard_api',
      'api_key',
    ]);

    if (!empty($api_key)) {
      $config->set('api_key', $api_key);
    }

    $guzzle_options = [
      'base_uri' => $form_state->getValue([
        'swapcard_api',
        'base_uri',
      ]),
      'timeout' => $form_state->getValue([
        'swapcard_api',
        'timeout',
      ]),
    ];

    $config->set('guzzle_options', $guzzle_options);

    // Save our configuration with values ready.
    $config->save();
    return parent::submitForm($form, $form_state);
  }

  /**
   * Ajax callback - upon first time entering API credentials.
   */
  public static function getEvents(&$form, FormStateInterface $form_state) {
    return NestedArray::getValue($form, ['swapcard_api']);
  }

  /**
   * Pint remote API to test the connection.
   *
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   FormState object.
   * @param Drupal\Core\Config\Config $config
   *   The config object belonging to this form.
   */
  protected function pingApi(FormStateInterface $form_state, Config $config) {

    $api_key = !empty($form_state->getValue(['swapcard_api', 'api_key'])) ? $form_state->getValue([
      'swapcard_api',
      'api_key',
    ]) : NULL;

    $base_uri = !empty($form_state->getValue(['guzzle_options', 'base_uri'])) ? $form_state->getValue([
      'guzzle_options',
      'base_uri',
    ]) : NULL;

    $status = [
      'errors' => [],
      'success' => [],
    ];

    if ($this->cache->get('swapcard_events') && $config->get('api_key') === $api_key) {
      $status['success'][] = $this->t('Access key valid, connection with API was successful.');
    }
    else {

      if ($api_key && !empty($base_uri)) {

        // Create "swapcard_event" plugin instance.
        $plugin = $this->swapcardPluginManager->createInstance('swapcard_event');

        $base_args = [
          'page' => 1,
          'pageSize' => 10,
        ];

        $options = [
          'body' => $plugin->queryString('events', $base_args),
          'headers' => [
            'Authorization' => '<' . $api_key . '>',
          ],
        ];

        // Send post request with Graphql query.
        $response = $plugin->post($base_uri, $options);

        if (isset($response['errors']) && !empty($response['errors'])) {
          $status['errors'] = $response['errors'];
        }

        if (is_array($response) && isset($response['data']) && count($response['data']) > 0) {

          // Set this into cache, so we do not "waste" number of api requests.
          if (isset($response['data']) && isset($response['data']['events'])) {
            $this->cache->set('swapcard_events', $response['data']['events']);
          }

          $status['success'][] = $this->t('Access key valid, connection with API was successful.');
        }
      }
    }

    // Report any eventual errors.
    if (!empty($status['errors'])) {
      foreach ($status['errors'] as $error) {
        $this->messenger()->addError($error['message']);
      }
    }
    // Otherwise print success message (api ping was ok).
    if (!empty($status['success'])) {
      foreach ($status['success'] as $success) {
        $this->messenger()->addStatus($success);
      }
    }

  }

}
