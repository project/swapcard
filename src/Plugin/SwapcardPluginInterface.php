<?php

namespace Drupal\swapcard\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Swapcard backends.
 *
 * Modules implementing this interface may want to extend Swapcard
 * class, which provides default implementations of each method.
 *
 * @see \Drupal\swapcard\Annotation\Swapcard
 * @see \Drupal\swapcard\Plugin\SwapcardPluginBase
 * @see \Drupal\swapcard\Plugin\SwapcardPluginManager
 * @see plugin_api
 */
interface SwapcardPluginInterface extends PluginInspectionInterface {

  /**
   * Renders json string ready for Swapcard API query.
   *
   * @param string $callback
   *   Main Graphql query callback, such as
   *   "events", "exhibitors", "meetings" etc.
   * @param array $callback_args
   *   Associative array of arguments for Graphql query callback, such as:
   *   "page", "pageSize", "eventId" etc.
   * @param array $fields
   *   An array with fields to include in the Graphql query request.
   * @param string $query_fields
   *   Additional input to query string that can be passed by reference.
   *
   * @return string
   *   Query string ready for the POST payload
   *   of reqeust to API (body param, such as:
   *   @code
   *   '{"query":"{ events(page: 1, pageSize: 50) { id title banner { imageUrl } language createdAt updatedAt }}}'
   *   @endcode
   */
  public function queryString(string $callback, array $callback_args = [], array $fields = [], string &$query_fields = '');

  /**
   * Generic method for various requests to Swapcard.
   *
   * @param string $uri
   *   API endpoint URL.
   * @param array $options
   *   Array with parameters that should extend default ones for GuzzleHttp.
   *
   * @return array
   *   An array with items retrieved from Swapcard endpoint.
   */
  public function post(string $uri, array $options = []);

}
