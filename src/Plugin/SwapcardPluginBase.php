<?php

namespace Drupal\swapcard\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use GuzzleHttp\Client;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a base class for Swapcard plugin.
 *
 * A complete sample plugin definition should be defined as in this example:
 *
 * @code
 * @Swapcard(
 *   id = "swapcard_exhibitors",
 *   admin_label = @Translation("Swapcard Exhibitors"),
 *   description = @Translation("Swapcard exhibitors query plugin"),
 *   fields = {
 *     "createdAt",
 *     "updatedAt",
 *     "htmlDescription",
 *     "banner" = {
 *       "imageUrl"
 *     },
 *     "categories"
 *   }
 * )
 * @endcode
 *
 * @see \Drupal\swapcard\Annotation\Swapcard
 * @see \Drupal\swapcard\Plugin\SwapcardPluginInterface
 * @see \Drupal\swapcard\Plugin\SwapcardPluginManager
 * @see plugin_api
 */
abstract class SwapcardPluginBase extends PluginBase implements SwapcardPluginInterface, ContainerFactoryPluginInterface {

  /**
   * Base fields for any Graphql query.
   *
   * Those should be available for all of the Graphql queries.
   *
   * @var \Drupal\swapcard\Plugin\SwapcardPluginBasebaseFields
   */
  public $baseFields = [
    'id',
    'description',
  ];

  /**
   * Base arguments for Graphql query callback.
   *
   * Those should be available for all of the Graphql queries.
   *
   * @var array
   */
  public $baseArgs = [];

  /**
   * Guzzle default options array.
   *
   * @var array
   */
  public $guzzleOptions = [
    'headers' => [
      'Content-Type' => 'application/json',
      'Accept' => 'application/json',
      'Authorization' => NULL,
    ],
    'base_uri' => NULL,
    'timeout' => NULL,
    'body' => NULL,
  ];

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Guzzle\Client instance.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Drupal\Core\Logger\LoggerChannelFactoryInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Drupal\Core\Extension\ModuleHandlerInterface definition.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs SwapcardPluginBase.
   *
   * @param array $configuration
   *   The configuration.
   * @param string $plugin_id
   *   The plugin ID for the migration process to do.
   * @param mixed $plugin_definition
   *   The configuration for the plugin.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Drupal config factory.
   * @param GuzzleHttp\Client $http_client
   *   GuzzleHttp vendor client.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   Drupal logger interface.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Drupal messenger interface.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   Module handler interface.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory, Client $http_client, LoggerChannelFactoryInterface $logger, MessengerInterface $messenger, ModuleHandlerInterface $module_handler) {

    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->configFactory = $config_factory;

    $config = $this->configFactory->get('swapcard.settings')->getRawData();

    if (!empty($config['guzzle_options'])) {
      $this->guzzleOptions = array_merge($this->guzzleOptions, $config['guzzle_options']);
    }

    if (!empty($config['api_key'])) {
      $this->guzzleOptions['headers']['Authorization'] = '<' . $config['api_key'] . '>';
    }

    $this->httpClient = new Client($this->guzzleOptions);
    $this->logger = $logger;
    $this->messenger = $messenger;
    $this->moduleHandler = $module_handler;
    $this->fields = (object) [];
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('http_client'),
      $container->get('logger.factory'),
      $container->get('messenger'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function queryString(string $callback, array $callback_args = [], array $fields = [], string &$query_fields = '') {

    // Deal with fields now, with some recursive "magic".
    if (empty($fields)) {

      if (isset($this->configuration['fields']) && !empty($this->configuration['fields'])) {
        $fields = array_merge($this->baseFields, $this->configuration['fields']);
      }
      else {
        $fields = !empty($this->configuration) && $this->configuration[0] == 'all' && isset($this->getPluginDefinition()['fields']) ? array_merge($this->baseFields, $this->getPluginDefinition()['fields']) : $this->baseFields;
      }
    }

    if (!empty($fields)) {

      // Add other modules ability to extend batches before final run.
      $this->moduleHandler->alter('swapcard_request', $fields, $callback, $callback_args);

      foreach ($fields as $key => $value) {

        if (is_array($value)) {

          $filtered[$key] = array_filter($value, function ($key) {
            return !is_numeric($key);
          }, ARRAY_FILTER_USE_KEY);

          if (empty($filtered[$key])) {
            $query_fields .= ' ' . $key . ' { ' . implode(' ', $value) . ' } ';
          }
          else {
            $query_fields .= ' ' . $key . ' { ';
            if (is_string($value)) {
              $query_fields .= $value;
            }
            elseif (is_array($value)) {
              $ready = array_filter($value, function ($key) {
                return is_numeric($key);
              }, ARRAY_FILTER_USE_KEY);
              if (!empty($ready)) {
                $query_fields .= implode(' ', $ready);
              }
            }

            // Run this all over.
            $this->queryString($callback, $callback_args, $filtered[$key], $query_fields);
            $query_fields .= ' } ';
          }
        }
        else {
          $query_fields .= ' ' . $value;
        }
      }
    }

    // Define query callback, with arguents included.
    if (!empty($callback_args)) {
      $this->baseArgs = array_merge($this->baseArgs, $callback_args);
    }

    $callback_args_strings = [];
    foreach ($this->baseArgs as $arg_id => $arg_value) {
      $callback_args_strings[] = $arg_id . ': ' . $arg_value;
    }
    $callback_string = $callback . '(' . implode(', ', $callback_args_strings) . ')';

    return '{"query":"{ ' . $callback_string . ' { ' . $query_fields . ' }}"}';
  }

  /**
   * {@inheritdoc}
   */
  public function post(string $uri, array $options = []) {
    try {

      $guzzle_options = !empty($options) ? array_merge($this->guzzleOptions, $options) : $this->guzzleOptions;

      // Do our http request via Guzzle.
      $response = $this->httpClient->post($uri, $guzzle_options);

      if ($response->getStatusCode() == 200) {
        $json = $response->getBody()->getContents();
        if (is_string($json) && substr($json, 0, 1) == '{') {
          // Return decoded data, as an array.
          return Json::decode($json);
        }
        else {
          return $json;
        }
      }
    }
    catch (\Exception $e) {
      $this->messenger->addError($e->getMessage());
    }
  }

}
