<?php

namespace Drupal\swapcard\Plugin\Swapcard;

use Drupal\swapcard\Plugin\SwapcardPluginBase;

/**
 * Swapcard events query plugin.
 *
 * Definition of a Swapcard events query plugin.
 *
 * @Swapcard(
 *   id = "swapcard_event",
 *   admin_label = @Translation("Swapcard Events"),
 *   description = @Translation("Swapcard events query plugin"),
 *   fields = {
 *     "title",
 *     "createdAt",
 *     "updatedAt",
 *     "htmlDescription",
 *     "language",
 *     "beginsAt",
 *     "endsAt",
 *     "canRegister",
 *     "timezone",
 *     "code",
 *     "visibility",
 *     "banner" = {
 *       "imageUrl"
 *     },
 *     "twitterHashtag",
 *     "translationLanguages",
 *     "totalSpeakers",
 *     "totalPlannings",
 *     "totalExhibitors",
 *     "totalExhibitorMembers",
 *     "slug",
 *     "showSimilarProducts",
 *     "showSimilarExhibitors",
 *     "isLive",
 *     "isPublic",
 *     "isSuggested",
 *     "address" = {
 *       "city",
 *       "country",
 *       "place",
 *       "state",
 *       "street",
 *       "zipCode"
 *     },
 *     "groups" = {
 *       "name",
 *       "peopleCount",
 *       "id",
 *       "exhibitorCount"
 *     },
 *     "longitude",
 *     "latitude"
 *   }
 * )
 */
class SwapcardEvents extends SwapcardPluginBase {}
