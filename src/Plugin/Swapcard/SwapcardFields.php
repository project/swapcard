<?php

namespace Drupal\swapcard\Plugin\Swapcard;

use Drupal\swapcard\Plugin\SwapcardPluginBase;

/**
 * Swapcard fields query plugin.
 *
 * Definition of a Swapcard fields query plugin.
 *
 * @Swapcard(
 *   id = "swapcard_fields",
 *   admin_label = @Translation("Swapcard Fields"),
 *   description = @Translation("Swapcard fields query plugin"),
 *   fields = {
 *     "title",
 *     "createdAt",
 *     "updatedAt",
 *     "fieldDefinitions" = {
 *       "... on SelectFieldDefinition" = {
 *         "id",
 *         "name",
 *         "optionsValues" = {
 *           "id",
 *           "translations" = {
 *             "value"
 *           },
 *           "value"
 *         }
 *       },
 *       "... on MultipleSelectFieldDefinition" = {
 *         "id",
 *         "name",
 *         "optionsValues" = {
 *           "id",
 *           "translations" = {
 *             "value"
 *           },
 *           "value"
 *         }
 *       }
 *     }
 *   }
 * )
 */
class SwapcardFields extends SwapcardPluginBase {}
