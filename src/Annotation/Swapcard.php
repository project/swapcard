<?php

namespace Drupal\swapcard\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Swapcard annotation object.
 *
 * @ingroup swapcard
 *
 * @Annotation
 */
class Swapcard extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The administrative label of the swapcard.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $admin_label = '';

  /**
   * The description of the swapcard.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description = '';

  /**
   * Graphql query fields, space separated.
   *
   * I.e. "id tite banner { imageUrl }" and similar.
   *
   * @var string
   */
  public $fields;

  /**
   * A parameter for most of the Graphql callbacks.
   *
   * @var string
   */
  public $eventId;

}
